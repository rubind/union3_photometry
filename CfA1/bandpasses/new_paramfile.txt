file            'ariess.fig1_Vthick.fits'

vertlines       406,402,0.05      414,1707,1.2
horzlines       514,1810,3500.	  2464,1810,11500.
vertoffset      0.
start           826,345
# Only the xvalue of 'start' has to be exact
outputfl        'FLWO_CfA1_Vthick.txt'

backval         65534.5
datasign        -1
logx            0
logy            0


convolve        0

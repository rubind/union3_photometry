from numpy import *
import pylab
from FileRead import readcol
from scipy.interpolate import interp1d
import glob


def file_to_lambs(fl):
    [l,t] = readcol(fl, 'ff')
    ifn = interp1d(l,t,kind = 'linear', bounds_error = False, fill_value = 0.)
    return ifn(lambs)

def get_vega_mag(bandpass, vega, source):
    return -2.5*log10(   (bandpass*source*lambs).sum() / (bandpass*vega*lambs).sum()   )

lambs = arange(3000., 11000.)

vega = file_to_lambs("../../../calspec/alpha_lyr_stis_006.ascii")
V = file_to_lambs("sv.dat")
I = file_to_lambs("si.dat")
v = file_to_lambs("FLWO_CfA1_Vthin.txt")
i = file_to_lambs("FLWO_CfA1_ithin.txt")

pickles = []
for fl in glob.glob("../../../pickles/*.dat"):
    pickles.append(file_to_lambs(fl))

for item in pickles:
    pylab.plot(get_vega_mag(V, vega, item) - get_vega_mag(I, vega, item),
               get_vega_mag(V, vega, item) - get_vega_mag(i, vega, item),
               '.', color = 'k')

    pylab.plot([0,1], [0, 1.06])



pylab.xlabel("V - I")
pylab.ylabel("V - i")
pylab.xlim(0,1)
pylab.ylim(0,1)
pylab.show()

from numpy import *
import pylab
from FileRead import readcol
from scipy.interpolate import interp1d
import glob


def file_to_lambs(fl, lambshift = 0.):
    [l,t] = readcol(fl, 'ff')
    ifn = interp1d(l + lambshift,t,kind = 'linear', bounds_error = False, fill_value = 0.)
    return ifn(lambs)

def sn_to_lambs(file_dat, redshift, phase):
    [p,l,f] = file_dat
    inds = where(p == phase)
    ifn = interp1d(l[inds]*(1. + redshift),f[inds],kind = 'linear', bounds_error = False, fill_value = 0.)
    return ifn(lambs)

def get_vega_mag(bandpass, vega, source):
    return -2.5*log10(   (bandpass*source*lambs).sum() / (bandpass*vega*lambs).sum()   )

lambs = arange(3000., 11000.)

#vega = file_to_lambs("../../../calspec/alpha_lyr_stis_006.ascii")
vega = file_to_lambs("../../../calspec/bd_17d4708_stisnic_004.ascii")
file_dat = readcol("../../../snflux_1a.dat", 'iff')

pickles = []
for fl in glob.glob("../../../pickles/*.dat"):
    pickles.append(file_to_lambs(fl))


red_filts = [file_to_lambs(item) for item in ["sv.dat", "sv.dat", "sr.dat", "si.dat"]]
blue_filts = [file_to_lambs(item) for item in ["sb.dat", "sb.dat", "sv.dat", "sv.dat"]]
nat_filts = [file_to_lambs(item) for item in ["FLWO_CfA1_Bthin.txt", "FLWO_CfA1_Vthin.txt", "FLWO_CfA1_Rthin.txt", "FLWO_CfA1_Ithin.txt"]]
bes_filts = [file_to_lambs(item, lambshift = 50.) for item in ["FLWO_CfA1_Bthin.txt", "FLWO_CfA1_Vthin.txt", "FLWO_CfA1_Rthin.txt", "FLWO_CfA1_Ithin.txt"]]
#bes_filts = [file_to_lambs(item) for item in ["sb.dat", "sv.dat", "sr.dat", "si.dat"]]




for subpl, red_filt, blue_filt, nat_filt, bes_filt in zip([1,2,3,4], red_filts, blue_filts, nat_filts, bes_filts):
    pylab.subplot(2,2,subpl)
    print subpl
    

    for item in pickles:
        pylab.plot(get_vega_mag(blue_filt, vega, item) - get_vega_mag(red_filt, vega, item),
                   get_vega_mag(nat_filt, vega, item) - get_vega_mag(bes_filt, vega, item),
                   '.', color = 'g')

    for redshift in (0.0, 0.051, 0.01):
        for phase in arange(-10, 16., 5.):
            item = sn_to_lambs(file_dat, redshift, phase)
            pylab.plot(get_vega_mag(blue_filt, vega, item) - get_vega_mag(red_filt, vega, item),
                       get_vega_mag(nat_filt, vega, item) - get_vega_mag(bes_filt, vega, item),
                       '.', color = 'b')



pylab.show()

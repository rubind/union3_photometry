import commands
from FileRead import readcol, writecol
from numpy import *

repl = {"Bthick": "521,345",
        "Bthin": "521,345",
        "Vthick": "826,345",
        "Vthin": "820,345",
        "Rthick": "992,345",
        "Rthin": "991,345",
        "Ithick": "1351,345",
        "Ithin": "1350,345"}

f = open("paramfile.txt")
orig_lines = f.read()
f.close()

for key in repl:
    print key
    lines = orig_lines.replace("sssss", repl[key]).replace("fffff", "ariess.fig1_%s.fits" % key).replace("ooooo", "FLWO_CfA1_%s.txt" % key)

    f = open("new_paramfile.txt", 'w')
    f.write(lines)
    f.close()
    
    #print commands.getoutput("python ../../../trace.py new_paramfile.txt")

    [l,t] = readcol("FLWO_CfA1_%s.txt" % key, 'ff')

    above_005 = t > 0.003

    cross_below = where((above_005[:-1] == 1)*(above_005[1:] == 0))[0][0]

    
    print cross_below
    above_005[cross_below:] = 0


    writecol("FLWO_CfA1_%s.txt" % key, [l[where(above_005)], t[where(above_005)]])

    

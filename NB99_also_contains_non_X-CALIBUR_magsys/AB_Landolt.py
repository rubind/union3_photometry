from Spectra import Spectra
from FileRead import file_to_fn, readcol
import numpy as np
import subprocess

whoami = subprocess.getoutput("whoami")

def get_AB_mag(SED, filt):
    return -2.5*np.log10(   (intlambs*SED(intlambs)*filt(intlambs)).sum()   /   (filt(intlambs)*0.10884806248/intlambs).sum()   )

def get_mean_AB(instrument, band, starkey):
    item = Spectra(instrument = instrument, band = band)
    filt = item.transmission_fn

    AB_mags = [get_AB_mag(SED, filt) for SED in calspec_fns[starkey]]

    assert len(calspec_fns[starkey]) == len(calspec_stars[starkey])
    return np.mean(AB_mags)


calspec_stars = {}
calspec_stars["Landolt"] = ["agk_81d266",
                            "bd26d2606",
                            "bd_75d325",
                            "feige110",
                            "feige34",
                            "g191b2b",
                            "gd153",
                            "gd71",
                            "grw_70d5824",
                            "hz4",
                            "hz44",
                            "lds749b",
                            "p177d",
                            "p330e"]

calspec_stars["Smith"] = ["bd02d3375", "bd21d0607", "bd26d2606",
                          "bd29d2091", "bd54d1216", "bd_75d325",
                          "p330e", "p177d", "feige34"]

calspec_stars["LandoltSmith"] = ["p330e", "p177d", "feige34", "bd26d2606", "bd_75d325"]


[NA, NA, NA, NA, NA, NA, NA, calspec_version] = readcol("../../../SCP_Stuff/calspec/all_opt.txt", 'aaaffaaa')

for key in calspec_stars:
    for i in range(len(calspec_stars[key])):
        count = 0
        for item in calspec_version:
            if "_".join(item.split("_")[:-2]) == calspec_stars[key][i]:
                count += 1
                calspec_stars[key][i] = item
        assert count == 1, calspec_stars[key][i]


calspec_fns = {}
for key in calspec_stars:
    calspec_fns[key] = []

    for calspec_star in calspec_stars[key]:
        print(("Loading ", calspec_star, key))
        calspec_fns[key].append(
            file_to_fn("/Users/" + whoami + "/Dropbox/SCP_Stuff/calspec/" + calspec_star + ".ascii", fill_value = 0., bounds_error = False)
        )

intlambs = np.arange(2000., 20000., 0.5)


Land_mags = dict(U = 11.4096, B = 12.2652, V = 12.3229, R = 12.3443, I = 12.3819) # Update when stars change!!!

for key in ["BV", "VR", "VI", "RI"]:
    Land_mags[key] = Land_mags[key[0]] - Land_mags[key[1]]

print("Land_mags", Land_mags)

Smith_mags = dict(u = 11.448, g = 10.7818, r = 10.6232, i = 10.6103)
for key in ["gr", "ri", "gi"]:
    Smith_mags[key] = Smith_mags[key[0]] - Smith_mags[key[1]]

print("Smith_mags", Smith_mags)

########################################## NB 99 ##########################################


color_terms = """1	-0.095	0.029	0.028	0.018
4	-0.097	0.016	0.006	0.022
5	0.133	0.033	0.067	0.00
6	0.055	0.02	0.03	0.053
7	0.226	-0.073	-0.106	0.034
8	0.094	-0.026	-0.052	0.018
10	0.094	-0.035	0.373	-0.041
11	0.048	0.048	0.048	-0.01
12	0.103	-0.021	0.029	0.023
13	0.105	0.002	-0.079	0.043
16	None	None	-0.296	-0.017""".split('\n')

# 1 = CTIO 1.5m SITE2K6
# 4 = CTIO 0.9m TK2
# 5 = DANISH DFOSC
# 6 = JKT Tek1
# 7 = LICK 1m DEWAR5
# 8 = LICK 1m DEWAR2
# 10 = YALO ANDICAM
# 11 = ESO 3.6m EFOSC
# 12 = KPNO 2.1m T1KA
# 13 = CFHT STIS2
# 16 = MARLY

for line in color_terms:
    parsed = line.split(None)
    
    if parsed[1] != "None":
        print("# %.3f - %.3f*(%.3f)" % (Land_mags["B"], Land_mags["BV"], float(parsed[1])))
        print("NB99  NB99_b" + parsed[0] + "  " + str(Land_mags["B"] - Land_mags["BV"]*float(parsed[1]) - get_mean_AB(instrument = "NB99", band = "NB99_b" + parsed[0], starkey = "Landolt")))
    if parsed[2] != "None":
        print("# %.3f - %.3f*(%.3f)" % (Land_mags["V"], Land_mags["BV"], float(parsed[2])))
        print("NB99  NB99_v" + parsed[0] + "  " + str(Land_mags["V"] - Land_mags["BV"]*float(parsed[2]) - get_mean_AB(instrument = "NB99", band = "NB99_v" + parsed[0], starkey = "Landolt")))

    print("# %.3f - %.3f*(%.3f)" % (Land_mags["R"], Land_mags["VR"], float(parsed[3])))
    print("NB99  NB99_r" + parsed[0] + "  " + str(Land_mags["R"] - Land_mags["VR"]*float(parsed[3]) - get_mean_AB(instrument = "NB99", band = "NB99_r" + parsed[0], starkey = "Landolt")))
    print("# %.3f - %.3f*(%.3f)" % (Land_mags["I"], Land_mags["VI"], float(parsed[4])))
    print("NB99  NB99_i" + parsed[0] + "  " + str(Land_mags["I"] - Land_mags["VI"]*float(parsed[4]) - get_mean_AB(instrument = "NB99", band = "NB99_i" + parsed[0], starkey = "Landolt")))
    
print()
print()

########################################## Landolt ##########################################


for instrument in ["CalanTololo", "Knop", "Krisciunas"]:
    for band in "UBVRI":
        print(instrument + "  Bessell12_" + band + "  " + str(Land_mags[band] - get_mean_AB(instrument = instrument, band = "Bessell12_" + band, starkey = "Landolt")))
    print()

########################################## CfA1 ##########################################


# b -> 12.2675, v -> 12.3211, r -> 12.342, i -> 12.3854 

for i, filt in enumerate("BVRI"):
    mean_CfA1_mag = [12.2675, 12.3211, 12.342, 12.3854][i] # Update when stars change!!!

    for thickthin in ["thick", "thin"]:
        this_band = "CfA1_FLWO_" + filt + "_" + thickthin
        print("CfA1_FLWO  " + this_band + "  " + str(mean_CfA1_mag - get_mean_AB(instrument = "CfA1_FLWO", band = this_band, starkey = "Landolt")))


print()
print()


########################################## CfA2 ##########################################

for i, filt in enumerate("UBVRI"):
    for band, mean_CfA2_mag in [("CfA2_AC_SAO_" + filt, [11.4706, 12.2673, 12.3209, 12.3419, 12.3843][i]), # Update when stars change!!!
                                ("CfA2_AC_Harris_" + filt, [11.4419, 12.2648, 12.3203, 12.3438, 12.3787][i]),
                                ("CfA2_4Sh1_SAO_" + filt, [11.4618, 12.2689, 12.3204, 12.3416, 12.3844][i]),
                                ("CfA2_4Sh3_SAO_" + filt, [11.444, 12.2697, 12.3206, 12.3413, 12.3838][i]),
                                ("CfA2_4Sh1_Harris_" + filt, [11.4428, 12.2675, 12.3203, 12.3434, 12.381][i]), # Hopefully close!
                                ("CfA2_4Sh3_Harris_" + filt, [11.4428, 12.2675, 12.3203, 12.3434, 12.381][i])]:

        print("CfA2_FLWO  " + band + "  " + str(mean_CfA2_mag - get_mean_AB(instrument = "CfA2_FLWO", band = band, starkey = "Landolt")))

print()
print()

########################################## CfA34 ##########################################


for filt, mag in [("U", 11.4213), ("B", 12.2695), ("V", 12.3209), ("R", 12.3442), ("I", 12.3809)]:
    this_filt = "CfA34_4Sh_Harris_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "Landolt")))

###

for filt, mag in [("U", 11.3892), ("B_1", 12.2687), ("V_1", 12.3218)]:
    this_filt = "CfA3" + "4"*(filt == "U") + "_KC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "Landolt")))

for filt, mag in [("r'_1", 11.3994), ("i'_1", 11.4675)]:
    this_filt = "CfA3_KC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "LandoltSmith")))

###

for filt, mag in [("B_1", 12.2679), ("B_2", 12.2712), ("V_1", 12.3215), ("V_2", 12.3215)]:
    this_filt = "CfA4_KC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "Landolt")))

for filt, mag in [("r'_1", 11.4004), ("r'_2", 11.4003), ("i'_1", 11.4689), ("i'_2", 11.4689)]:
    this_filt = "CfA4_KC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "LandoltSmith")))

###

for filt, mag in [("B", 12.2688), ("V", 12.3207)]:
    this_filt = "CfA3_MC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "Landolt")))

for filt, mag in [("r'", 11.4035), ("i'", 11.4729)]:
    this_filt = "CfA3_MC_" + filt
    print("CfA34_FLWO  " + this_filt + "  " + str(mag - get_mean_AB(instrument = "CfA34_FLWO", band = this_filt, starkey = "LandoltSmith")))


########################################## CSP DR3 ##########################################

print("SWOPE  SWOPE_B" + "  " + str(12.2687 - get_mean_AB(instrument = "SWOPE", band = "SWOPE_B", starkey = "Landolt")))  # Update when stars change!!!

for Vsuffix, Vmag in [("-LC-3014", 11.3886), ("-LC-3009", 11.3895), ("-LC-9844", 11.3886)]:  # Update when stars change!!!
    print("SWOPE  SWOPE_V" + Vsuffix + "  " + str(Vmag - get_mean_AB(instrument = "SWOPE", band = "SWOPE_V" + Vsuffix, starkey = "LandoltSmith")))

for band, mag in zip("ugri", [11.4174, 10.784, 10.6234, 10.6104]):  # Update when stars change!!!
    print("SWOPE  SWOPE_" + band + "  " + str(mag - get_mean_AB(instrument = "SWOPE", band = "SWOPE_" + band, starkey = "Smith")))

print()
print()

########################################## LOSS ##########################################

LOSS_color_terms = """K1	-0.095	0.027	-0.181	-0.071
K2	-0.085	0.032	0.062	-0.007
K3	-0.057	0.032	0.064	-0.001
K4	-0.134	0.051	0.107	0.014
N1	-0.092	0.053	0.089	-0.044
N2       0.042  0.082   0.092   -0.044""".split('\n')

LOSS_band_terms = ["BV", "BV", "VR", "VI"]
    
for i in range(4):
    this_band = "BVRI"[i]
    
    for line in LOSS_color_terms:
        parsed = line.split(None)
        color_coeff = float(parsed[i+1])

        print("LOSS  LOSS_%s_%s  %f" % (parsed[0],
                                        this_band,
                                        Land_mags[this_band] + color_coeff*Land_mags[LOSS_band_terms[i]]
                                        - get_mean_AB(instrument = "LOSS", band = "LOSS_%s_%s" % (parsed[0],
                                                                                                  this_band), starkey = "Landolt")
                                        ))
########################################## LOSS ##########################################

    
for this_band, RI_color_term in [("MOSAICII_R", -0.030), ("MOSAICII_I", 0.022)]:    
    print("MOSAICII  %s  %f" % (this_band,
                                Land_mags[this_band[-1]] + RI_color_term*(Land_mags["RI"] - 0.32)
                                - get_mean_AB(instrument = "MOSAICII", band = this_band, starkey = "Landolt")
    ))



########################################## LSQ+LCO ##########################################

for this_band, color_name, color_term in [("B", "BV", -0.024), ("V", "BV", -0.014), ("g", "gr", 0.109), ("r", "ri", 0.027), ("i", "ri", 0.036)]:
    if color_name == "BV":
        std_color = Land_mags["BV"]
        std_mag = Land_mags[this_band]
        this_starkey = "Landolt"
        
    elif color_name == "gr" or color_name == "ri":
        std_color = Smith_mags[color_name]
        std_mag = Smith_mags[this_band]
        this_starkey = "Smith"
    else:
        assert 0
        
    print("LCOGT  LCOGT_%s  %f" % (this_band,
                                   std_mag + color_term*std_color - get_mean_AB(instrument = "LCOGT", band = "LCOGT_" + this_band, starkey = this_starkey)
    ))

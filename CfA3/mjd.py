from numpy import *
import pylab
from FileRead import readcol

[NA, mjd] = readcol("cfa3lightcurves.naturalsystem.txt", 'if')

pylab.hist(mjd, bins = arange(mjd.min() - 4., mjd.max() + 4.))

pylab.plot([53218]*2, [0, 100])
pylab.plot([53552]*2, [0, 100])

pylab.show()

import numpy as np
from numpy import *
import glob
from FileRead import readcol, clean_lines, read_by_space, read_param
import subprocess
import ephem
from scipy.interpolate import interp1d
import time
import sys
from astropy.io import ascii, fits
from Spectra import get_zCMB, get_zhelio
import json
import sfdmap

whoami = subprocess.getoutput("whoami")

################################# General Routines ################################

def is_number(thestring):
    try:
        float(thestring)
        return True
    except:
        return False

"""
def get_SFD98(ra_deg, dec_deg):
    
    [radec, sfd98] = readcol("NED_info_SFD98.txt", 'af')
	
    key = str(ra_deg) + "_" + str(dec_deg)
    if radec.count(key):
        return sfd98[radec.index(key)]
	
    output = subprocess.getoutput("curl 'http://ned.ipac.caltech.edu/cgi-bin/calc?in_csys=Equatorial&in_equinox=J2000.0&obs_epoch=2000&lon=%fd&lat=%fd&pa=0.0&out_csys=Equatorial&out_equinox=J2000.0' | grep Landolt\ [BV]" % (ra_deg, dec_deg)).split('\n')
    
    assert output[-2].split(None)[:2] == ["Landolt", "B"], "Weird output!\n" + str(output)
    assert output[-1].split(None)[:2] == ["Landolt", "V"], "Weird output!\n" + str(output)
    A_B = float(output[-2].split(None)[7])
    A_V = float(output[-1].split(None)[7])

    f = open("NED_info_SFD98.txt", 'a')
    f.write(key + "  " + str(A_B - A_V) + '\n')
    f.close()
    time.sleep(1.1)

    return A_B - A_V
"""

def get_SFD98(ra_dec, dec_deg):
    return SFD.ebv(ra_dec, dec_deg)*0.86

def get_RA_Dec(snname):

    [sne, sneRA, sneDec] = readcol("NED_info_SNRADEC.txt", 'aff')
    
    if sne.count(snname):
        return sneRA[sne.index(snname)], sneDec[sne.index(snname)]

    output = subprocess.getoutput("curl 'http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=%s&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES#Positions_0' | grep Equatorial | grep J2000 | grep -v \<" % snname).split('\n')
   
    parsed = output[-1].split(None)
    
    assert parsed[0] == "Equatorial", "Weird output!\n" + str(output)
    assert parsed[1] == "(J2000.0)", "Weird output!\n" + str(output)

    f = open("NED_info_SNRADEC.txt", 'a')
    f.write(snname + "  " + str(parsed[2]) + "  " + str(parsed[3]) + '\n')
    f.close()

    time.sleep(1.1)
    return float(parsed[2]), float(parsed[3])

def do_it(cmd):
    print(cmd)
    print(subprocess.getoutput(cmd))

def read_paramfile(flname):
    opts = {}
    [Dataset, Include, IncludeuU, UuMagsys, BVRIgriMagsys, OtherMagsys] = readcol(flname, 'aiiaaa')
    
    for i in range(len(Dataset)):
        opts[Dataset[i]] = dict(Include = Include[i], IncludeuU = IncludeuU[i], UuMagsys = UuMagsys[i], BVRIgriMagsys = BVRIgriMagsys[i], OtherMagsys = OtherMagsys[i])
    print("Options Read")
    for key in opts:
        print(opts[key])


    f = open(flname, 'r')
    lines = f.read().split('\n')
    f.close()

    for line in lines:
        parsed = line.split(None)
        if len(parsed) > 1:
            if parsed[0][0] == "@":
                opts[parsed[0][1:]] = parsed[1:]

    return opts


def get_val_from_column(flnm, snname, column, sep = '\t'):
    f = open(flnm)
    lines = f.read().split('\n')
    f.close()

    for line in lines:
        parsed = line.split(sep)
        try:
            parsed[column]
            if parsed[0] == snname:
                return parsed[column]
        except:
            pass
    return None

def snname_to_standard_snname(snname):
    if snname[:2] == "20" or snname[:2] == "19" or snname[:2] == "18":
        return "SN" + snname
    if snname[0] == "9" or snname[0] == "8":
        return "SN19" + snname
    if snname[0] == "1" or snname[0] == "0":
        return "SN20" + snname
    return snname

def append_phot(phot_list, set_name, instrument, filt, magsys, mjd, flux, dflux, zeropoint):

    this_key = (set_name, instrument, filt, magsys)
    if this_key not in phot_list:
        phot_list[this_key] = {"mjd": array([], dtype=float64),
                               "flux": array([], dtype=float64),
                               "dflux": array([], dtype=float64),
                               "zeropoint": array([], dtype=float64)}

    phot_list[this_key]["mjd"] = append(phot_list[this_key]["mjd"], mjd)
    phot_list[this_key]["flux"] = append(phot_list[this_key]["flux"], flux)
    phot_list[this_key]["dflux"] = append(phot_list[this_key]["dflux"], dflux)
    phot_list[this_key]["zeropoint"] = append(phot_list[this_key]["zeropoint"], zeropoint)

    return phot_list


def merge_other(the_data, sn, key_name, key_value, tolerance):
    if key_name in the_data[sn]:
        assert abs(the_data[sn][key_name] - key_value) < tolerance, "Failure comparing " + " ".join([sn,  key_name, str(the_data[sn][key_name]), str(key_value), str(the_data[sn])])

        print("Merging ", sn, key_name, key_value, the_data[sn][key_name])
        the_data[sn][key_name] = 0.5*(key_value +  the_data[sn][key_name])
    else:
        the_data[sn][key_name] = key_value
        print("Creating ", sn, key_name)
    return the_data


def make_lc2_files(phot_list, wd):
    
    for key in phot_list:
        if len(key[0]) > 1:
            (set_name, instrument, filt, magsys) = key

            f = open(wd + "/lc2fit_" + filt + ".dat", 'w')
            f.write("#Date  :\n")
            is_mag = any(phot_list[key]["zeropoint"] == -1)
            if is_mag:
                f.write("#Mag  :\n")
                f.write("#Magerr  :\n")
                f.write("#end  :\n")
            else:
                f.write("#Flux  :\n")
                f.write("#Fluxerr  :\n")
                f.write("#ZP  :\n")
                f.write("#end  :\n")
            f.write("@INSTRUMENT  " + instrument  + "\n")
            f.write("@BAND  " + filt + "\n")
            f.write("@MAGSYS  " + magsys + "\n")
            
            
            for mjd, flux, dflux, zeropoint in zip(phot_list[key]["mjd"], phot_list[key]["flux"], phot_list[key]["dflux"], phot_list[key]["zeropoint"]):
                to_write = [mjd, flux, dflux] + [zeropoint]*(1 - is_mag)
                to_write = [str(item) for item in to_write]
                f.write("  ".join(to_write) + '\n')
            f.close()

    f = open(wd + "/lightfile", 'w')
    for key in ("RA", "Dec", "z_CMB", "z_heliocentric", "SFD98"):
        f.write(key.replace("SFD98", "MWEBV") + "  " + str(phot_list[key])) # + '\n')
        try:
            f.write("  " + str(phot_list[key + "_unc"]))
        except:
            pass
        f.write('\n')
                    
        
    if "mass" in phot_list:
        if phot_list["mass"] > 11.9:
            print("Large mass found!!!" + str(phot_list["mass"]) + " " + wd)
            time.sleep(1)
        f.write("Mass  %.2f  %.2f  %.2f\n" % (phot_list["mass"], -1.*abs(phot_list["mass-"]), phot_list["mass+"]))
    else:
        f.write("Mass  %.2f  %.2f  %.2f\n" % (0., -1, 1.))

    f.close()

def get_set_name(phot_list):
    set_weights = {}


    for key in phot_list:
        if len(key[0]) > 1:
            (set_name, instrument, filt, magsys) = key
            print(key)
            if set_name not in set_weights:
                print("Found ", set_name)
                set_weights[set_name] = 0.

            data_point_weights = (phot_list[key]["flux"]/phot_list[key]["dflux"])**2. * (phot_list[key]["zeropoint"] != -1) + (1./phot_list[key]["dflux"]**2.)*(2.5/log(10.))**2. * (phot_list[key]["zeropoint"] == -1)
            
            data_point_weights = clip(data_point_weights, 0, 10000.)
            data_point_weights[where(isinf(data_point_weights))] = 10000. # Clip inf weight
            data_point_weights[where(isnan(data_point_weights))] = 10000. # Clip inf weight, not sure which of these lines will catch it.
            set_weights[set_name] += sum(data_point_weights)
            
    print("set_weights ", set_weights)
    max_sum = 0.
    for key in set_weights:
        if set_weights[key] > max_sum:
            max_sum = set_weights[key]
            highest_weight_set = key

    print("Highest Weight", highest_weight_set)
    return highest_weight_set

################################# Specific Datasets ################################

def parse_CalanTololo(the_data):
    RAs = "19:59:02.28 03:37:22.64 21:34:58.12 10:29:27.79 13:23:22.20 20:00:08.65 10:09:00.30 13:10:04.20 19:23:42.29 12:42:48.95 21:28:17.66 13:24:10.12 01:29:08.04 23:04:34.76 00:10:40.27 03:05:17.28 07:41:56.53 04:59:27.55 03:43:01.90 23:15:13.25 01:21:58.44 03:36:37.95 01:45:44.83 03:29:27.20 10:34:51.38 13:52:50.34 19:13:01.53 13:31:07.87 23:10:54.09 05:08:00.71 10:03:35.00 23:51:50.27 17:15:35.92 20:45:56.45".split(None)
    Decs = "-56:15:30.0 -33:02:40.1 -62:44:07.4 +22:00:46.4 -26:06:28.7 -55:22:03.4 -26:38:24.4 -46:26:30.3 -62:49:30.1 +10:21:37.5 -61:33:00.0 -23:52:39.3 -32:16:30.0 -37:20:42.1 -49:56:43.3 -39:33:39.7 -62:31:08.8 -58:49:44.2 -53:37:56.8 -44:44:34.5 -34:12:43.5 -18:21:13.7 -56:05:57.9 -37:16:18.9 -34:26:30.0 -30:42:23.3 -64:17:28.3 -33:12:50.5 -44:58:48.6 -37:29:18.0 -35:27:47.6 -27:57:47.0 +16:19:25.8 -51:23:40.0".split(None)
    zhelios = "0.0404  0.0391  0.0506  0.0546  0.0317  0.0141  0.0446  0.0103  0.037  0.0252  0.0752  0.0249 -1  0.1018  0.0614  0.0202 0.0352  0.0450  0.0581  0.0437  0.0189  0.0793  0.0882  0.0637  0.0696  0.0239  0.090  0.0510  0.088  0.0034  0.0490  0.0297 0.0303 0.0146".split(None)
    SNe = "1990T  1990Y 1990af  1991S  1991U  1991ag  1992J 1992K  1992O 1992P  1992ae  1992ag  1992ai  1992aq  1992au  1992bc  1992bg 1992bh 1992bk 1992bl 1992bo 1992bp 1992br 1992bs 1993B  1993H 1993M  1993O  1993T 1993af 1993ag 1993ah 1990O  1992al".split(None)
    
    assert len(SNe) == len(zhelios), "Mismatched lengths! " + str(len(SNe)) + " " + str(len(zhelios))
    assert len(SNe) == len(RAs), "Mismatched lengths!"
    assert len(SNe) == len(Decs), "Mismatched lengths!"

    for i in range(len(SNe))[::-1]:
    	zhelios[i] = float(zhelios[i])
    	if float(zhelios[i]) < -0.1:
            print("Deleting ", SNe[i])
            del RAs[i]
            del Decs[i]
            del zhelios[i]
            del SNe[i]
    
    
    [SN_list, jd_list, B_list, dB_list, V_list, dV_list, R_list, dR_list, I_list, dI_list] = read_by_space("CalanTololo/table4.dat",
                                                                                         		           "1993ab 2449072.73 17.174 0.030 16.739 0.030 16.616 0.030 16.840 0.031",
                                                                                                		   'afffffffff', missingval = "999999")
     
    SN_list = [item.strip() for item in SN_list]                                                                                              
    for i in range(len(SN_list)):
    	if SN_list[i] != "999999":
            current_sn = "SN" + SN_list[i]
            print("Current SN: ", current_sn)
            sn_ind = SNe.index(SN_list[i])

            if current_sn not in the_data:
                    the_data[current_sn] = {}


                    the_data = merge_other(the_data, current_sn, "z_heliocentric", zhelios[sn_ind], 0.001)
                    the_data = merge_other(the_data, current_sn, "RA", float(ephem.Equatorial(RAs[sn_ind], Decs[sn_ind]).ra)*180./pi, 0.01)
                    the_data = merge_other(the_data, current_sn, "Dec", float(ephem.Equatorial(RAs[sn_ind], Decs[sn_ind]).dec)*180./pi, 0.01)

            the_data = merge_other(the_data, current_sn, "SFD98", get_SFD98(the_data[current_sn]["RA"], the_data[current_sn]["Dec"]), 0.002)
            the_data = merge_other(the_data, current_sn, "z_CMB", get_zCMB(the_data[current_sn]["RA"], the_data[current_sn]["Dec"], the_data[current_sn]["z_heliocentric"])
                                                       , 0.0005)


            for filt, mag, dmag in [("B", B_list, dB_list), ("V", V_list, dV_list), ("R", R_list, dR_list), ("I", I_list, dI_list)]:
                    if mag[i] < 99:
                            the_data[current_sn] = append_phot(
                                    the_data[current_sn], set_name = "CalanTololo", instrument = "CalanTololo", filt = "Bessell12" + "_" + filt, magsys = opts["CalanTololo"]["BVRIgriMagsys"],
                                    mjd = jd_list[i] - 2400000.5, flux = mag[i], dflux = dmag[i], zeropoint = -1.
                            )
    return the_data



def parse_CfA1(the_data):

    color_terms = {"B": ("BV", 0.04), # B - b = 0.04*(B - V)
                   "V": ("BV", -0.03),
                   "R": ("VR", -0.1075), #-0.08),
                   "I": ("VI", 0.06)}

    color_found = 0
    color_miss = 0

    for lcfl in glob.glob("CfA1/1*txt"):
        snname = "SN" + lcfl.split("/")[-1].replace(".txt", "")


        if snname not in the_data:
            the_data[snname] = {}

        RA, Dec = get_RA_Dec(snname)
        the_data = merge_other(the_data, snname, "RA", RA, 0.01)
        the_data = merge_other(the_data, snname, "Dec", Dec, 0.01)
        the_data = merge_other(the_data, snname, "z_heliocentric", 3.33564095e-6 * 10.**float(get_val_from_column("CfA1/980366.tb3.txt", snname[2:], column = 1, sep = None))
                               , 0.0005)
        the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(RA, Dec, the_data[snname]["z_heliocentric"])
                               , 0.0005)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(RA, Dec), 0.002)




        f = open(lcfl)
        lines = f.read().split('\n')
        f.close()

        for line in lines:
            parsed = line.split(None)
            if len(parsed) > 2:
                # This step is amazing:
                mjd = float(parsed[0].replace(",", "")) + 2440000. - 2400000.5
                    
                ind = 1
                filts = "B,V,R,I".split(",")
                mags = {}
                dmags = {}

                while ind < len(parsed):
                    if parsed[ind] == "\ldots":
                        ind += 1
                        del filts[0]
                    else:
                        mag = float(parsed[ind])
                        dmag = float(parsed[ind+1].replace("(", "").replace(")", "")
                        )
                        ind += 2
                        mags[filts[0]] = mag
                        dmags[filts[0]] = dmag
                        del filts[0]

                        assert dmag < 0.5, "Color term too uncertain! " + str(parsed)


                
                for filt in "BVRI":

                    try:
                        mags[filt]

                        try:
                            mags[color_terms[filt][0][0]]
                            mags[color_terms[filt][0][1]]
                            
                            color_corr = (mags[color_terms[filt][0][0]] - mags[color_terms[filt][0][1]])*color_terms[filt][1]
                            color_found += 1
                        except:
                            color_corr = 0.
                            color_miss += 1

                        if 1:#opts.CfA1nat:
                            if opts["XCALIBURShiftMagsys"].count(opts["CfA1"]["BVRIgriMagsys"]):
                                this_instrument = "XCfA1_FLWO"
                            elif opts["NoShiftMagSys"].count(opts["CfA1"]["BVRIgriMagsys"]):
                                this_instrument = "CfA1_FLWO"
                            else:
                                assert 0

                            this_filt = "CfA1_FLWO_" + filt + "_thick"*(mjd < 49929) + "_thin"*(mjd >= 49929)
                        else:
                            this_instrument = "STANDARD"
                            this_filt = filt
                            assert opts.XCAL == 0, "Can't currently use X-CALIBUR with STANDARD!"

                        the_data[snname] = append_phot(
                            the_data[snname], set_name = "CfA1", instrument = this_instrument, filt = this_filt, magsys = opts["CfA1"]["BVRIgriMagsys"],
                            mjd = mjd, flux = mags[filt] - color_corr*1., dflux = dmags[filt], zeropoint = -1. # opts.CfA1nat fixed to 1 for color_corr.
                        )
                    except:
                        pass
    print("color_miss fraction for CfA1", color_miss/float(color_miss + color_found))

    for key in the_data:
        print(key)
        print(the_data[key])

    return the_data

def parse_CfA2(the_data):
    [NA, SN_list, jd_list, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, instr_list] = read_by_space("CfA2/datafile4.txt",
                                                                                               "SN 1999ac 2451275.90 16.888 0.047 16.430 0.022 15.319 0.009 14.901 0.024 14.712 0.017 4sh-chip3/Harris+I_{SAO}",
                                                                                               'aafffffffffffa', missingval = "999999")
    # Missingval should be large enough that, even when multiplied by a color term, |magnitudes| > 99

    instr_list = [item.strip() for item in instr_list]
    jd_list = list(jd_list)

    # Note: All of these are positive.

    [SN_z, NA, zhelio] = readcol("CfA2/204512.tb6.txt", 'aaf')
    zhelio /= 299792.458
    SN_z = [snname_to_standard_snname(item) for item in SN_z]
    
    for fl in glob.glob("CfA2/jha06.natural/*phot.dat"):
        snname = snname_to_standard_snname(fl.split("/sn")[1].split(".")[0])
        print("snname ", snname)
        
        
        if snname not in the_data:
            the_data[snname] = {}

        RA, Dec = get_RA_Dec(snname)
        the_data = merge_other(the_data, snname, "RA", RA, 0.01)
        the_data = merge_other(the_data, snname, "Dec", Dec, 0.01)
        the_data = merge_other(the_data, snname, "z_heliocentric", zhelio[SN_z.index(snname)]
                               , 0.0005)
        the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(RA, Dec, the_data[snname]["z_heliocentric"])
                               , 0.0005)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(RA, Dec), 0.002)

        [sn_jd, U, dU, B, dB, V, dV, R, dR, I, dI] = readcol(fl, 'f,ff,ff,ff,ff,ff')
        mags = {"U": U, "B": B, "V": V, "R": R, "I": I}
        dmags = {"U": dU, "B": dB, "V": dV, "R": dR, "I": dI}
        
        sn_jd += 2400000. # SIGH!
        sn_mjd = sn_jd - 2400000.5
        
        for i in range(len(sn_jd)):
            for filt in "UBVRI":
                if mags[filt][i] < 50:
                    assert jd_list.count(sn_jd[i]) == 1, "Couldn't find date " +  str(sn_jd[i]) + " " + str(fl)

                    instr = instr_list[jd_list.index(sn_jd[i])]
                    
                    if instr == "AndyCam/SAO":
                        filt_name = "CfA2_AC_SAO_" + filt
                    elif instr == "4Sh-chip1/SAO":
                        filt_name = "CfA2_4Sh1_SAO_" + filt
                    elif (instr == "4Sh-chip3/SAO") or (instr == "4sh-chip3/SAO"):
                        filt_name = "CfA2_4Sh3_SAO_" + filt
                    elif instr == "4Sh-chip1/Harris":
                        filt_name = "CfA2_4Sh1_Harris_" + filt
                    elif instr == "4sh-chip3/Harris":
                        filt_name = "CfA2_4Sh3_Harris_" + filt
                    elif instr == "AndyCam/Harris":
                        filt_name = "CfA2_AC_Harris_" + filt

                    elif instr == "4sh-chip3/Harris+I_{SAO}":
                        if filt != "I":
                            filt_name = "CfA2_4Sh3_Harris_" + filt
                        else:
                            filt_name = "CfA2_4Sh3_SAO_" + filt
    
                    elif instr == "AndyCam/Harris+I_{SAO}":
                        if filt != "I":
                            filt_name = "CfA2_AC_Harris_" + filt
                        else:
                            filt_name = "CfA2_AC_SAO_" + filt

                    else:
                        print("Unknown instrument! "*100, "'" + instr  + "'")
                        stop_here

                    if filt == "U":
                        magsys = opts["CfA2"]["UuMagsys"]
                        instrument = "CfA2_FLWO"
                    else:
                        if opts["XCALIBURShiftMagsys"].count(opts["CfA2"]["BVRIgriMagsys"]):
                            instrument = "XCfA2_FLWO"
                        elif opts["NoShiftMagSys"].count(opts["CfA2"]["BVRIgriMagsys"]):
                            instrument = "CfA2_FLWO"
                        else:
                            assert 0

                        magsys = opts["CfA2"]["BVRIgriMagsys"]

                    the_data[snname] = append_phot(
                         the_data[snname], set_name = "CfA2", instrument = instrument, filt = filt_name, magsys = magsys,
                                           mjd = sn_mjd[i], flux = mags[filt][i], dflux = dmags[filt][i], zeropoint = -1.
                                           )

        
    return the_data

def parse_NB99(the_data):

    zhelio_dict = {"SN1999aa": 0.014205,
                   "SN1999ac": 0.009188,
                   "SN1999ao": 0.05393,
                   "SN1999ar": 0.15488,
                   "SN1999aw": 0.038,
                   "SN1999bi": 0.12272,
                   "SN1999bm": 0.14282,
                   "SN1999bn": 0.12851,
                   "SN1999bp": 0.07706}

                           

    for sn in glob.glob("NB99_also_contains_non_X-CALIBUR_magsys/1999*"):
        snname = "SN" + sn.split("/")[-1]
        
        print("snname ", snname)
        
        
        if snname not in the_data:
            the_data[snname] = {}

        RA, Dec = get_RA_Dec(snname)
        the_data = merge_other(the_data, snname, "RA", RA, 0.01)
        the_data = merge_other(the_data, snname, "Dec", Dec, 0.01)
        the_data = merge_other(the_data, snname, "z_heliocentric", zhelio_dict[snname]
                               , 0.0005)
        the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(RA, Dec, the_data[snname]["z_heliocentric"])
                               , 0.0005)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(RA, Dec), 0.002)

        for lc2fl in glob.glob(sn + "/lc2fit_[ubvri]*dat"):

            band = read_param(lc2fl, "@BAND")
            instr = read_param(lc2fl, "@INSTRUMENT")

            [date, flux, dflux, zp] = readcol(lc2fl, 'ffff')
            date += 2451000.
            date -= 2400000.5

            assert len(date) > 0, "Bad file! " + lc2fl
            
            the_data[snname] = append_phot(
                the_data[snname], set_name = "NB99", instrument = instr, filt = band, magsys = opts["NB99"]["BVRIgriMagsys"],
                mjd = date, flux = flux, dflux = dflux, zeropoint = zp,
            )
    return the_data


def parse_krisciunas(the_data):
    # Krisciunas et al. 2000, 2001, 2003, 2004a,b, 2006
    
    # heading key: 0:U 1:B 2:V 3:R 4:I 5:B-V 6:V-R 7:V-I, 10:dU, 11:dB, 12:dV, 13:dR, 14:dI large_number:JD_offset -1:Ignore
    headings = {"SN1999aa": [2451000., 2, -1,  5, -1,  6, -1,  7],
                "SN1999cc": [2451000., 1, -1,  2, -1,  3, -1,  4],
                "SN1999cl": [2451000., -1, 1, -1,  2, -1,  3, -1,  4],
                "SN1999cp": [2451000., -1, 2, -1,  5, -1,  6, -1,  7],
                "SN1999da": [2451000., -1, 2, -1,  5, -1,  6, -1,  7],
                "SN1999dk": [2451000., 2, -1,  5, -1,  6, -1,  7],
                "SN1999ek": [2451000., 1, -1,  2, -1,  3, -1,  4],
                "SN1999gp": [2451000., 2, -1,  5, -1,  6, -1,  7],
                "SN2000bh": [2451000., 0, -1,  1, -1,  2, -1,  3, -1,  4],
                "SN2000bk": [2451000., 2, -1,  5, -1,  6, -1,  7],
                "SN2000ca": [2451000., 0, -1,  1, -1,  2, -1,  3, -1,  4],
                "SN2000ce": [2451000., 2, -1,  5, -1,  6, -1,  7],
                "SN2000cf": [2451000., 1, -1,  2, -1,  3, -1,  4],
                "SN2001ba": [2450000., 1, -1,  2, -1,  4],
                "SN2001bt": [2450000., 1, -1,  2, -1,  3, -1,  4],
                "SN2001cn": [2450000., 0, -1,  1, -1,  2, -1,  3, -1,  4],
                "SN2001cz": [2450000., 0, -1,  1, -1,  2, -1,  3, -1,  4],
                "SN2001el": [2450000., -1, -1, 0, -1,  1, -1, 11, 2, -1, 12,  3, -1,  4],
                "SN2002bo": [2452000., 0, -1, 10,  1, -1, 11, 2, -1, 12, 3, -1, 13, 4, -1, 14]}

    zhelio_dict = {"SN1999aa": 0.014443,
                   "SN1999cc": 0.031328,
                   "SN1999cl": 0.007609,
                   "SN1999cp": 0.009480,
                   "SN1999da": 0.012695,
                   "SN1999dk": 0.014960,
                   "SN1999ek": 0.017522,
                   "SN1999gp": 0.026745,
                   "SN2000bh": 0.022809,
                   "SN2000bk": 0.025444,
                   "SN2000ca": 0.023616,
                   "SN2000ce": 0.016305,
                   "SN2000cf": 0.036425,
                   "SN2001ba": 0.029557,
                   "SN2001bt": 0.014637,
                   "SN2001cn": 0.015154,
                   "SN2001cz": 0.015489,
                   "SN2001el": 0.003896,
                   "SN2002bo": 0.004240}

    for snfl in glob.glob("Krisciunas/SN??????.txt") + glob.glob("Krisciunas/SN?????.txt"): # The second one should be empty, but why not?
        snname = snfl.split("/")[-1].replace(".txt", "")

        if snname not in the_data:
            the_data[snname] = {}

        RA, Dec = get_RA_Dec(snname)
        the_data = merge_other(the_data, snname, "RA", RA, 0.01)
        the_data = merge_other(the_data, snname, "Dec", Dec, 0.01)
        the_data = merge_other(the_data, snname, "z_heliocentric", zhelio_dict[snname]
                               , 0.0005)
        the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(RA, Dec, the_data[snname]["z_heliocentric"])
                               , 0.0005)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(RA, Dec), 0.002)


        f = open(snfl)
        lines = f.read()
        f.close()

        lines = lines.replace("(", "").replace(")", "").replace("\ldots", "X X ").replace(",", "").replace("[", "").replace("]", "")
        lines = lines.split('\n')


        for line in lines:
            parsed = line.split(None)
            if any([is_number(item) for item in parsed]):
                phot_dict = {}
                for i, heading in enumerate(headings[snname]):
                    if is_number(parsed[i]):
                        if heading > 100:
                            phot_dict["mjd"] = float(parsed[i]) + heading - 2400000.5
                        elif heading == 0:
                            phot_dict["U"] = float(parsed[i])
                            phot_dict["dU"] = float(parsed[i+1])
                        elif heading == 1:
                            phot_dict["B"] = float(parsed[i])
                            phot_dict["dB"] = float(parsed[i+1])
                        elif heading == 2:
                            phot_dict["V"] = float(parsed[i])
                            print(parsed)
                            phot_dict["dV"] = float(parsed[i+1])
                        elif heading == 3:
                            phot_dict["R"] = float(parsed[i])
                            phot_dict["dR"] = float(parsed[i+1])
                        elif heading == 4:
                            phot_dict["I"] = float(parsed[i])
                            phot_dict["dI"] = float(parsed[i+1])
                        elif heading == 5:
                            phot_dict["B"] = float(parsed[i]) + phot_dict["V"]
                            phot_dict["dB"] = sqrt(float(parsed[i+1])**2. + phot_dict["dV"]**2.)
                        elif heading == 6:
                            phot_dict["R"] = -float(parsed[i]) + phot_dict["V"]
                            phot_dict["dR"] = sqrt(float(parsed[i+1])**2. + phot_dict["dV"]**2.)
                        elif heading == 7:
                            phot_dict["I"] = -float(parsed[i]) + phot_dict["V"]
                            phot_dict["dI"] = sqrt(float(parsed[i+1])**2. + phot_dict["dV"]**2.)
                        elif heading == 10:
                            phot_dict["U"] += float(parsed[i])
                        elif heading == 11:
                            phot_dict["B"] += float(parsed[i])
                        elif heading == 12:
                            phot_dict["V"] += float(parsed[i])
                        elif heading == 13:
                            phot_dict["R"] += float(parsed[i])
                        elif heading == 14:
                            phot_dict["I"] += float(parsed[i])

                
                for key in phot_dict:
                    if key != "mjd" and key.count("d") == 0:
                        the_data[snname] = append_phot(
                            the_data[snname], set_name = "Krisciunas", instrument = "Krisciunas", filt = "Bessell12_" + key, magsys = opts["Krisciunas"]["BVRIgriMagsys"],
                            mjd = phot_dict["mjd"], flux = phot_dict[key], dflux = phot_dict["d" + key], zeropoint = -1.
                        )
            else:
                print("Skipping ", line)
    
    #fdafdsafds # pause to check all Skipping
    return the_data

def parse_LOSS(the_data):
    # 2001dl           52121.33  B  17.921  0.084  KAIT2 
    # JD 2452121.83, so no half-day offset

    [sn_list, mjd_list, filt_list, mag_list, dmag_list, instrument_list] = readcol("LOSS/loss.phot.natural.table.txt", 'a,fa,ff,a')

    sn_list = [snname_to_standard_snname(item) for item in sn_list]
    sn_set = list(set(sn_list))
	
    instrument_list = [item.replace("KAIT", "K").replace("NICKEL", "N1") for item in instrument_list]

    #f = open("LOSS/apjs341025t9_ascii.txt") Low precision, so don't use!
    #lines = f.read()
    #f.close()
    #lines = lines.replace(" ", "").split('\n')
	
	

    for sn in sn_set:
        if sn not in the_data:
            the_data[sn] = {}

            if sn == "SN2001bg":
                RA, Dec = 128.82858333,	28.46827778
            elif sn == "SNF20080909-030":
                RA, Dec = 330.45445833, 13.05530556
            elif sn == "SNF20071021-000":
                RA, Dec = 3.74929167, 16.33500000
            else:
                print("Getting RA Dec for ", sn)
                RA, Dec = get_RA_Dec(sn)

            the_data = merge_other(the_data, sn, "RA", RA, 0.01)
            the_data = merge_other(the_data, sn, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

            z_helio, z_helio_unc = match_list_of_hosts(sn)

            if z_helio == -1:
                assert 0, "Couldn't find host galaxy for " +  sn

            the_data = merge_other(the_data, sn, "z_heliocentric", z_helio, 0.001)
            the_data = merge_other(the_data, sn, "z_heliocentric_unc", z_helio_unc, 0.001)
            the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"])
                                   , 0.001)
            

        for i in range(len(sn_list)):
            if sn_list[i] == sn and mag_list[i] < 30: # LOSS apparently includes points with 99.990 for some reason
                the_data[sn] = append_phot(
                    the_data[sn], set_name = "LOSS", instrument = "LOSS", filt = "LOSS_" + instrument_list[i] + "_" + filt_list[i], magsys = opts["LOSS"]["BVRIgriMagsys"],
                mjd = mjd_list[i], flux = mag_list[i], dflux = dmag_list[i], zeropoint = -1.
                )

					
    return the_data

def parse_LOSS2(the_data):
    [sn_list, mjd_list, B_list, dB_list, V_list, dV_list, R_list, dR_list, I_list, dI_list, NA, NA, instrument_list] = readcol("LOSS/LOSS Stahl/TableB3.txt", 'a,f,ff,ff,ff,ff,ff,a')
    instrument_list = [item.replace("kait", "K").replace("nickel", "N") for item in instrument_list]
    
    sn_list = [snname_to_standard_snname(item) for item in sn_list]
    sn_set = list(set(sn_list))

    all_mags = dict(B = B_list, dB = dB_list,
                    V = V_list, dV = dV_list,
                    R = R_list, dR = dR_list,
                    I = I_list, dI = dI_list)
    
    for sn in sn_set:
        if sn not in the_data:
            the_data[sn] = {}
            
            print("Getting RA Dec for ", sn)
            RA, Dec = get_RA_Dec(sn)
            
            the_data = merge_other(the_data, sn, "RA", RA, 0.01)
            the_data = merge_other(the_data, sn, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

            z_helio, z_helio_unc = match_list_of_hosts(sn)

            if z_helio == -1:
                assert 0, "Couldn't find host galaxy for " +  sn

            the_data = merge_other(the_data, sn, "z_heliocentric", z_helio, 0.001)
            the_data = merge_other(the_data, sn, "z_heliocentric_unc", z_helio_unc, 0.001)

            the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"])
                                   , 0.001)

            
        for i in range(len(sn_list)):
            for band in "BVRI":
                if sn_list[i] == sn and all_mags[band][i] < 30: # LOSS apparently includes points with 99.990 for some reason
                    the_data[sn] = append_phot(
                        the_data[sn], set_name = "LOSS", instrument = "LOSS", filt = "LOSS_" + instrument_list[i] + "_" + band, magsys = opts["LOSS"]["BVRIgriMagsys"],
                        mjd = mjd_list[i], flux = all_mags[band][i], dflux = all_mags["d" + band][i], zeropoint = -1.
                    )
    return the_data

def parse_CNIa02(the_data):
    [sn_list, JD, mag, dmag, filt, NA, NA, instrument] = readcol("CNIa0.02/apjsac50b7t3_mrt.txt", 'a,fff,a,aa,a')

    
    sn_list = [snname_to_standard_snname(item) for item in sn_list]
    sn_set = list(set(sn_list))	

    for sn in sn_set:
        if sn not in the_data:
            the_data[sn] = {}

            print("Getting RA Dec for ", sn)
            RA, Dec = get_RA_Dec(sn)

            the_data = merge_other(the_data, sn, "RA", RA, 0.01)
            the_data = merge_other(the_data, sn, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

            z_helio, z_helio_unc = match_list_of_hosts(sn)            

            if z_helio == -1:
                assert 0, "Couldn't find host galaxy for " +  sn

            the_data = merge_other(the_data, sn, "z_heliocentric", z_helio, 0.001)
            the_data = merge_other(the_data, sn, "z_heliocentric_unc", z_helio_unc, 0.001)

            the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"])
                                   , 0.001)
            

        
        for i in range(len(sn_list)):
            if sn_list[i] == sn:

                if "BVRI".count(filt[i]):
                    the_filt = "Bessell12" + "_" + filt[i]
                    the_magsys = opts["CNIa02"]["BVRIgriMagsys"]
                elif "gri".count(filt[i]):
                    the_filt = "SDSS" + "_" + filt[i]
                    the_magsys = "AB"
                else:
                    assert 0
                    
                instrument = "CNIa02"

                zeropoint = 25.
                the_flux = mag[i]
                the_dflux = dmag[i]
                
                if dmag[i] > 3:
                    # upper_limit
                    the_flux = 0.
                    the_dflux = 10.**(0.4*(zeropoint - mag[i])) # 5 sigma upper limit on paper, but seems more like 1 sigma
                else:
                    the_flux = 10.**(0.4*(zeropoint - mag[i]))
                    the_dflux = 0.92103403719*dmag[i]*the_flux
                    
                the_data[sn] = append_phot(
                    the_data[sn], set_name = "CNIa02", instrument = instrument, filt = the_filt, magsys = the_magsys,
                mjd = JD[i] - 2400000.5, flux = the_flux, dflux = the_dflux, zeropoint = zeropoint
                )
    
    return the_data


def parse_CfA3(the_data):
    sig_pipe_mags =  [10.0,  16.0,  16.5,  17.0,  17.5,  18.0,  1.85,  19.0,  19.5,  20.0,  20.5,  21.5,  30.0]
    sig_pipe = {"U": [0.020, 0.020, 0.030, 0.048, 0.073, 0.105, 0.144, 0.191, 0.245, 0.306, 0.374, 0.450, 0.450],
                'u': [0.020, 0.020, 0.038, 0.058, 0.081, 0.107, 0.136, 0.168, 0.203, 0.240, 0.281, 0.324, 0.324],
                'B': [0.012, 0.012, 0.016, 0.024, 0.037, 0.053, 0.074, 0.099, 0.128, 0.162, 0.199, 0.241, 0.241],
                'V': [0.010, 0.010, 0.013, 0.019, 0.028, 0.041, 0.058, 0.077, 0.100, 0.126, 0.156, 0.189, 0.189],
                'R': [0.010, 0.010, 0.012, 0.018, 0.026, 0.037, 0.052, 0.070, 0.091, 0.115, 0.142, 0.172, 0.172],
                'I': [0.010, 0.010, 0.013, 0.019, 0.028, 0.041, 0.056, 0.075, 0.097, 0.122, 0.150, 0.181, 0.181],
                'r': [0.010, 0.010, 0.012, 0.018, 0.026, 0.037, 0.052, 0.070, 0.091, 0.115, 0.142, 0.172, 0.172],
                'i': [0.010, 0.010, 0.013, 0.019, 0.028, 0.041, 0.056, 0.075, 0.097, 0.122, 0.150, 0.181, 0.181],
                }

    sig_pipe_fns = {}
    
    for key in sig_pipe:
        sig_pipe_fns[key] = interp1d(sig_pipe_mags, sig_pipe[key], kind = 'linear')
    
    f = open("CfA3/cfa3lightcurves.naturalsystem.txt")
    lines = f.read().split('\n')
    lines = clean_lines(lines)
    f.close()


    for line in lines:
        if line[:2] == "sn":
            current_sn = "SN20" + line[2:]
            print("Current SN ", current_sn)
            
            if current_sn not in the_data:
                the_data[current_sn] = {}

        
            RAsex, Decsex = get_val_from_column("CfA3/apj309428t1_ascii.txt", current_sn[2:], 3, sep = '\t').split(None)
            RA = float(ephem.Equatorial(RAsex, Decsex).ra)*180./pi
            Dec = float(ephem.Equatorial(RAsex, Decsex).dec)*180./pi

            zcmb = get_val_from_column("CfA3/apj309428t8_ascii.txt", current_sn[4:], 1, sep = None)
            if zcmb == None:
                print("zCMB was None")
                [NA, zSN, NA, NA, zhel_list, NA] = read_by_space("LOSS/table1.dat",
                                                                 "SN 1994Bc Ia-norm [P94a]a081751.35+155320.5aaabbbcccddde 26682 0.052",
                                                                 'aaaaff', missingval = "999999")
                zSN = [item.strip() for item in zSN]
                
                if zSN.count(current_sn[2:]):
                    zhelio = zhel_list[zSN.index(current_sn[2:])]/299792.458
                    print("Read ", current_sn, "from LOSS")
                else:
                    print("Couldn't find ", current_sn, "reading from other_z")
                    zhelio = float(get_val_from_column("CfA3/other_z.txt", current_sn, 1, sep = None))

                zcmb = get_zCMB(RA, Dec, zhelio)
            else:
                zcmb = float(zcmb)
                zhelio = get_zhelio(RA, Dec, zcmb)

            the_data = merge_other(the_data, current_sn, "RA", RA, 0.01)
            the_data = merge_other(the_data, current_sn, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, current_sn, "SFD98", get_SFD98(the_data[current_sn]["RA"], the_data[current_sn]["Dec"]), 0.002)
            the_data = merge_other(the_data, current_sn, "z_heliocentric", zhelio, 0.0005)
            the_data = merge_other(the_data, current_sn, "z_CMB", zcmb, 0.0005)

        else:
            parsed = line.split(None)
            mjd = float(parsed[1])
            mag = float(parsed[2])
            dmag = float(parsed[3])

            filt = {"1": "U", "2": "B", "3": "V", "4": "R", "5": "I", "13": "r'", "14": "i'"}[parsed[0]]



            sig_pipe_mag = sig_pipe_fns[filt[0]](mag)

            if filt == "U" or filt == "u'":
                magsys = opts["CfA3"]["UuMagsys"]
                instrument = "CfA34_FLWO"
            else:
                if opts["XCALIBURShiftMagsys"].count(opts["CfA3"]["BVRIgriMagsys"]):
                    instrument = "XCfA34_FLWO"
                elif opts["NoShiftMagSys"].count(opts["CfA3"]["BVRIgriMagsys"]):
                    instrument = "CfA34_FLWO"
                else:
                    assert 0

                magsys = opts["CfA3"]["BVRIgriMagsys"]

            if mjd < 53218:
                filt = "CfA34_4Sh_Harris_" + filt
            elif mjd < 53580 :
                filt = "CfA3_MC_" + filt
            else:
                if (filt != "U") and (filt != "u'"):
                    filt = "CfA3_KC_" + filt + "_1"
                else:
                    filt = "CfA34_KC_" + filt

            

            if opts["CfA3"]["IncludeuU"] or (parsed[0] != "1"):
                the_data[current_sn] = append_phot(
                    the_data[current_sn], set_name = "CfA3", instrument = instrument, filt = filt, magsys = magsys,
                    mjd = mjd, flux = mag, dflux = sqrt(dmag**2. + sig_pipe_mag**2. ), zeropoint = -1.
                    # filt[0] drops "_one"
                )

    return the_data



def parse_CfA4(the_data):
    [sn_list, filt_list, mjd_list, NA, NA, NA, mag_list, dmag_list, period_list] = readcol("CfA4/cfa4.lc.natsystem.sort.ascii.txt", 'aaf,fff,ff,a')
    [sn2_list, RA_sex_list, Dec_sex_list, NA, z_hel_list, z_CMB_list, MWEBV_list] = readcol("CfA4/apjs430138t1_ascii.txt", 'a,aa,a,fff')

    sn_list = [snname_to_standard_snname(item) for item in sn_list]
    sn2_list = [snname_to_standard_snname(item) for item in sn2_list]


    sn_set = list(set(sn_list + sn2_list))


    for sn in sn_set:
        if sn not in the_data:
            the_data[sn] = {}

        for i in range(len(sn_list)):
            if sn_list[i] == sn:

                if filt_list[i] == "U" or filt_list[i] == "u'":
                    magsys = opts["CfA4"]["UuMagsys"]
                    instrument = "CfA34_FLWO"
                    period_txt = ""
                    pre_name = "CfA34"
                else:
                    if opts["XCALIBURShiftMagsys"].count(opts["CfA4"]["BVRIgriMagsys"]):
                        instrument = "XCfA34_FLWO"
                    elif opts["NoShiftMagSys"].count(opts["CfA4"]["BVRIgriMagsys"]):
                        instrument = "CfA34_FLWO"
                    else:
                        assert 0

                    magsys = opts["CfA4"]["BVRIgriMagsys"]
                    period_txt = "_" + (period_list[i] == "one")*"1" + (period_list[i] == "two")*"2"
                    pre_name = "CfA4"

                if opts["CfA4"]["IncludeuU"] or (not (filt_list[i] == "u'" or filt_list[i] == "U")):
                    the_data[sn] = append_phot(
                        the_data[sn], set_name = "CfA4", instrument = instrument, filt = pre_name + "_KC_" + filt_list[i] + period_txt, magsys = magsys,
                        mjd = mjd_list[i], flux = mag_list[i], dflux = dmag_list[i], zeropoint = -1.
                    )


        z_helio, z_helio_unc = match_list_of_hosts(sn)

        matched = 0
        for i in range(len(sn2_list)):
            if sn2_list[i] == sn:
                the_data = merge_other(the_data, sn, "RA", float(ephem.Equatorial(RA_sex_list[i], Dec_sex_list[i]).ra)*180./pi, 0.01)
                the_data = merge_other(the_data, sn, "Dec", float(ephem.Equatorial(RA_sex_list[i], Dec_sex_list[i]).dec)*180./pi, 0.01)

                the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

                if z_helio > 0:
                    the_data = merge_other(the_data, sn, "z_heliocentric", z_helio, 0.001)
                    the_data = merge_other(the_data, sn, "z_heliocentric_unc", z_helio_unc, 0.001)
                else:
                    the_data = merge_other(the_data, sn, "z_heliocentric", z_hel_list[i], 0.001)

                the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"])
                                       , 0.001)
                
                matched += 1

        assert matched == 1, "Matched error" +  str(matched) + " " + sn

    return the_data



def CSP_band_to_band_name(band, mjd):
    if band == 'V':
        if mjd < 53749:
            band_name = "V-LC-3014"
        elif mjd < 53760:
            band_name = "V-LC-3009"
        else:
            band_name = "V-LC-9844"
    else:
        band_name = band
    return band_name

def parse_CSP(the_data):
    for fl in glob.glob("CSP_Photometry_DR2/*dat"):
        print(fl)
        filters = subprocess.getoutput("grep MJD " + fl).split(None)
        assert len(filters)/2. - 1 == filters.count("+/-"), "Problem parsing " + str(filters) + " " + fl

        [mjd_list, NA, NA, NA, NA, NA, NA] = readcol(fl, 'f,ff,ff,ff')
        
        filters = filters[2::2]
        print("filters", filters)

        phot_list = {}
        file_data = loadtxt(fl)
        for filt in set('uVBgri').intersection(filters):
            ind = filters.index(filt)*2 + 1
            phot_list[filt] = (file_data[:,ind], file_data[:,ind+1])

        sn = fl.split("/")[-1].split("opt+nir")[0]
        
        if sn not in the_data:
            the_data[sn] = {}

        for i in range(len(mjd_list)):
            for band in phot_list:
                band_name = CSP_band_to_band_name(band, mjd_list[i])

                if band_name == "u":
                    magsys = opts.UuMagsys
                    instrument = "SWOPE"

                else:
                    pass



                if phot_list[band][0][i] < 99:
                    the_data[sn] = append_phot(the_data[sn], set_name = "CSP", instrument = instrument, filt = "SWOPE_" + band_name,
                                               magsys = magsys, mjd = mjd_list[i], flux = phot_list[band][0][i], dflux = phot_list[band][1][i], zeropoint = -1.
                                           )
        
        f = open(fl)
        lines = f.read().split('\n')
        f.close()

        for line in lines:
            if line.count("zcmb = "):
                parsed = line.split(None)

        if parsed[1] != "zcmb" or parsed[4] != "RA":
            print("Error parsing ", fl)
            sys.exit(1)

        if float(parsed[3]) != 0.:
            the_data = merge_other(the_data, sn, "z_CMB", float(parsed[3]), 0.0015)
        
        z_helio = get_val_from_column("CSP_Photometry_DR2/aj404219t1_ascii.txt", sn[2:], column = 5)
        if z_helio == None:
            z_helio = get_val_from_column("CSP_Photometry_DR2/aj319147t1_ascii.txt", sn[2:], column = 5)
        
        assert z_helio != None, "Couldn't find " + sn

        the_data = merge_other(the_data, sn, "z_heliocentric", float(z_helio.replace("dagger", "").replace("Dagger", "").replace("^mnplus", "")), 0.0015)
        the_data = merge_other(the_data, sn, "RA", float(ephem.Equatorial(parsed[6], parsed[9]).ra)*180./pi, 0.01)
        the_data = merge_other(the_data, sn, "Dec", float(ephem.Equatorial(parsed[6], parsed[9]).dec)*180./pi, 0.01)
        the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"]), 0.0015)
        the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)
    return the_data


def parse_CSP3(the_data):
    [CSP_SN, CSP_band, CSP_date, CSP_mag, CSP_dmag] = readcol("CSP3/DR3/SN_photo.dat", 'aafff')
    CSP_band = [item.replace("V0", "V").replace("V1", "V") for item in CSP_band]

    CSP_date += 2453000. - 2400000.5

    CSP_SN = array(CSP_SN)
    CSP_band = array(CSP_band)

    for sn in unique(CSP_SN):
        if sn not in the_data:
            the_data[sn] = {}

        inds = where(CSP_SN == sn)[0]

        for i in inds:
            band_name = CSP_band_to_band_name(CSP_band[i], CSP_date[i])

            if band_name == "u":
                if opts["CSP3"]["IncludeuU"]:
                    magsys = opts["CSP3"]["UuMagsys"]
                    instrument = "SWOPE"

            else:
                if opts["XCALIBURShiftMagsys"].count(opts["CSP3"]["BVRIgriMagsys"]):
                    instrument = "XSWOPE"
                elif opts["NoShiftMagSys"].count(opts["CSP3"]["BVRIgriMagsys"]):
                    instrument = "SWOPE"
                else:
                    assert 0, "Couldn't find MagSys " + opts["CSP3"]["BVRIgriMagsys"]

                magsys = opts["CSP3"]["BVRIgriMagsys"]

            if CSP_mag[i] < 99 and CSP_mag[i] > 0 and 'ugriBV'.count(CSP_band[i]):
                the_data[sn] = append_phot(the_data[sn], set_name = "CSP", instrument = instrument, filt = "SWOPE_" + band_name,
                                           magsys = magsys, mjd = CSP_date[i], flux = CSP_mag[i], dflux = CSP_dmag[i], zeropoint = -1.
                                       )
        

        z_helio = read_param("CSP3/DR3/" + sn + "_snpy.txt", sn, ind = 1)
        RA = read_param("CSP3/DR3/" + sn + "_snpy.txt", sn, ind = 2)
        Dec = read_param("CSP3/DR3/" + sn + "_snpy.txt", sn, ind = 3)

        the_data = merge_other(the_data, sn, "z_heliocentric", float(z_helio), 0.01)
        the_data = merge_other(the_data, sn, "RA", RA, 0.05)
        the_data = merge_other(the_data, sn, "Dec", Dec, 0.05)
        the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"]), 0.01)
        the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.02)
    return the_data


def parse_Foundation(the_data):
    [param_SN, param_zhelio] = readcol("Foundation/param.tex", 'af')
    [coords_SN, NA, coords_RA, coords_Dec] = readcol("Foundation/Foundation_RA_Dec.dat", 'aaff')
    [LC_SN, LC_MJD, LC_filt, LC_mag, LC_dmag, LC_flux, LC_dflux] = readcol("Foundation/lc.ascii", 'afaffff')
    LC_SN = np.array(LC_SN)

    zeropoints = LC_mag + 2.5*log10(LC_flux)
    possible_upper_limit_inds = where(abs(zeropoints - 27.5) > 0.01)
    assert all(LC_dmag[possible_upper_limit_inds] > 0.25)
    
    
    for sn in np.unique(LC_SN):
        snname = snname_to_standard_snname(sn)
        
        if snname not in the_data:
            the_data[snname] = {}

        param_ind = param_SN.index(sn)
        coords_ind = coords_SN.index(sn)
        inds = where(LC_SN == sn)[0]

        for ind in inds:
            the_data[snname] = append_phot(the_data[snname], set_name = "Foundation", instrument = "PS", filt = "PS_" + LC_filt[ind].replace("P1", ""),
                                           magsys = "AB", mjd = LC_MJD[ind],
                                           flux = LC_flux[ind], dflux = LC_dflux[ind], zeropoint = 27.5)

        
        the_data = merge_other(the_data, snname, "z_heliocentric", param_zhelio[param_ind], 0.01)
        the_data = merge_other(the_data, snname, "RA", coords_RA[coords_ind], 0.05)
        the_data = merge_other(the_data, snname, "Dec", coords_Dec[coords_ind], 0.05)
        the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(the_data[snname]["RA"], the_data[snname]["Dec"], the_data[snname]["z_heliocentric"]), 0.01)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(the_data[snname]["RA"], the_data[snname]["Dec"]), 0.02)
        

    return the_data
    

def parse_LSQ(the_data):
    outliers = array([55987.17725, 56234.18497, 56234.19028, 56234.19964, 56234.19523])
    outl_found = 0

    the_SN_table = ascii.read("LSQ/table3.dat", delimiter = "|")

    the_phot_table = ascii.read("LSQ/table2.dat")
    SN_list = [item.strip() for item in the_SN_table["SN"]]

    for i in range(len(SN_list)):
        sn = SN_list[i]
        if sn not in the_data:
            the_data[sn] = {}

        inds = where(the_phot_table["SN"] == sn)[0]

        for ind in inds:
            band_name = CSP_band_to_band_name(the_phot_table["band"][ind], the_phot_table["JD"][ind] - 2400000.5)

            if min(abs((the_phot_table["JD"][ind] - 2400000.5) - outliers)) > 0.0001:
                the_data[sn] = append_phot(the_data[sn], set_name = "LSQ", instrument = "SWOPE", filt = "SWOPE_" + band_name,
                                           magsys = opts["LSQ"]["BVRIgriMagsys"], mjd = the_phot_table["JD"][ind] - 2400000.5,
                                           flux = the_phot_table["mag"][ind], dflux = the_phot_table["dmag"][ind], zeropoint = -1.
                                       )
            else:
                outl_found += 1


        the_data = merge_other(the_data, sn, "z_CMB", the_SN_table["zcmb"][i], 0.0015)
        the_data = merge_other(the_data, sn, "z_heliocentric", the_SN_table["zhelio"][i], 0.0015)
        the_data = merge_other(the_data, sn, "RA", float(ephem.Equatorial(the_SN_table["RA"][i], the_SN_table["DEC"][i]).ra)*180./pi, 0.01)
        the_data = merge_other(the_data, sn, "Dec", float(ephem.Equatorial(the_SN_table["RA"][i], the_SN_table["DEC"][i]).dec)*180./pi, 0.01)
        the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"]), 0.0015)
        the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

    assert outl_found == len(outliers)
    return the_data



def parse_LSQLCO(the_data):
    [sn_list, filt_list, mjd_list, mag_list, dmag_list] = readcol("LSQ+LCO/paspabd417t7_mrt.txt", 'aafff')

    sn_list = np.array([snname_to_standard_snname(item) for item in sn_list])
    sn_set = list(set(sn_list))

    for sn in sn_set:
        if sn not in the_data:
            the_data[sn] = {}
            
            print("Getting RA Dec for ", sn)
            RA, Dec = get_RA_Dec(sn)
            
            the_data = merge_other(the_data, sn, "RA", RA, 0.01)
            the_data = merge_other(the_data, sn, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, sn, "SFD98", get_SFD98(the_data[sn]["RA"], the_data[sn]["Dec"]), 0.002)

            z_helio, z_helio_unc = match_list_of_hosts(sn)

            if z_helio == -1:
                #do_it("/Applications/Firefox.app/Contents/MacOS/firefox https://www.wis-tns.org/object/" + sn.replace("SN", ""))
                do_it("/Applications/Firefox.app/Contents/MacOS/firefox https://simbad.cds.unistra.fr/simbad/sim-id?Ident=" + sn + "&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id")
                
                assert 0, "Couldn't find host galaxy for " +  sn

            the_data = merge_other(the_data, sn, "z_heliocentric", z_helio, 0.001)
            the_data = merge_other(the_data, sn, "z_heliocentric_unc", z_helio_unc, 0.001)

            the_data = merge_other(the_data, sn, "z_CMB", get_zCMB(the_data[sn]["RA"], the_data[sn]["Dec"], the_data[sn]["z_heliocentric"])
                                   , 0.001)

            
        inds = np.where(sn_list == sn)
        for ind in inds[0]:
            assert sn_list[ind] == sn
            the_data[sn] = append_phot(
                        the_data[sn], set_name = "LSQ+LCO", instrument = "LCOGT", filt = "LCOGT_" + filt_list[ind], magsys = opts["LSQ+LCO"]["BVRIgriMagsys"],
                        mjd = mjd_list[ind], flux = mag_list[ind], dflux = dmag_list[ind], zeropoint = -1.
                    )
    return the_data


def parse_SDSS(the_data):
    outliers = {"SN2007ri": ("i", 54412.284112),
                "SN2006nu": ("r", 54031.186637),
                "SN2006fi": ("g", 53996.241756),
                "SN2006hj": ("i", 54032.123438),
                "SN2006js": ("i", 54041.113089)
                }

    [CID, RA, Dec, NA, IAUname, SNclass, NA, NA, NA, NA, NA, z_helio, NA, z_CMB] = readcol("JLA/sdsssn_master.dat2.txt", 'iffiaa,ffiii,fff')
    
    RA = RA % 360.

    f = open("JLA/sdsssn_master.dat2.txt")
    lines = f.read().split('\n')
    f.close()

    mass = []
    mass_low = []
    mass_high = []

    for line in lines:
        parsed = line.split(None)
        if len(parsed) > 100 and line.count(".") > 5:

            try:
                mass.append(float(parsed[127]))
                mass_low.append(float(parsed[128]))
                mass_high.append(float(parsed[129]))
            except:
                mass.append(0.)
                mass_low.append(-1.)
                mass_high.append(1.)

    mass = array(mass)
    mass_low = array(mass_low)
    mass_high = array(mass_high)


    IAUname = array(IAUname)
    SNclass = array(SNclass)

    # set(SNclass)
    # set(['AGN', 'zSNIa', 'SNIc', 'SNIa', 'Unknown', 'zSNII', 'pSNIbc', 'SNIb', 'SLSN', 'pSNIa', 'pSNII', 'SNIa?', 'SNII', 'Variable', 'zSNIbc'])

    inds = where((SNclass == "SNIa") | (SNclass == "SNIa?"))

    RA = RA[inds]
    Dec = Dec[inds]
    IAUname = IAUname[inds]

    for item in IAUname:
        if len(item) > 4 and sum(IAUname == item) > 1:
            print(item, " found more than once!")
            sys.exit(1)

    SNclass = SNclass[inds]
    z_helio = z_helio[inds]
    z_CMB = z_CMB[inds]
    mass = mass[inds]
    mass_low = mass_low[inds]
    mass_high = mass_high[inds]
    CID = CID[inds]

    change_in_AB_offsets_from_Sako = dict(u = 0.0848 - 0.0679, g = -0.0164 + 0.0203, r = -0.0067 + 0.0049, i = -0.0206 + 0.0178, z = -0.0142 + 0.0102)
    # From Sako: The AB offsets should be added to the native magnitudes to obtain magnitudes calibrated to the AB system. Signs - + + + + Fluxes are expressed in μJ and have the AB offsets already applied.
    # Data Release = (SDSS + old_offsets) e.g., uSDSS - 0.0679
    # Want uSDSS - 0.0848 instead
    # So, subtract above from Data Release zeropoints
    
    for i in range(len(RA)):

        snname = IAUname[i]
        if len(snname) < 4:
            snname = str(CID[i])
            snname = "-SDSS" + snname
        snname = "SN" + snname

        if snname not in the_data:
            the_data[snname] = {}

        smp_file = "JLA/SMP_Data/SMP_%06i.dat" % CID[i]
        f = open(smp_file)
        line = f.read().split('\n')[0]
        f.close()

        """
        Well, errors in SMP files. Can't do this!
        assert line.count("IAU:"), "'IAU:' not found in " + smp_file
        if len(IAUname[i]) > 4:
            assert line.count(IAUname[i]), "IAU name " + IAUname[i] + " not found in " + smp_file
            # Capitalization standard means that 2005A won't match 2005ab
        """

        [flag, mjd, filt, NA, NA, NA, NA, flux, fluxerr] = readcol(smp_file, 'ifi,ffff,ff')
        inds = where(flag < 1024)

        flag = flag[inds]
        mjd = mjd[inds]
        filt = filt[inds]
        flux = flux[inds]
        fluxerr = fluxerr[inds]

        
        for j in range(len(filt)):
            good_point = 1
            if snname in outliers:
                if outliers[snname][0] == "ugriz"[filt[j]]:
                    if abs(outliers[snname][1] - mjd[j]) < 0.01:
                        good_point = 0
                
            if good_point:
                the_data[snname] = append_phot(the_data[snname], set_name = "SDSS", instrument = "SDSS", filt = "SDSS_" + "ugriz"[filt[j]],
                                               magsys = "AB", mjd = mjd[j], flux = flux[j], dflux = fluxerr[j], zeropoint = 23.9 - change_in_AB_offsets_from_Sako["ugriz"[filt[j]]]  )# + ab_offsets["ugriz"[filt[j]]])
                
            
        the_data = merge_other(the_data, snname, "z_heliocentric", z_helio[i], 0.003)
        the_data = merge_other(the_data, snname, "z_CMB", z_CMB[i], 0.003)
        the_data = merge_other(the_data, snname, "RA", RA[i], 0.01)
        the_data = merge_other(the_data, snname, "Dec", Dec[i], 0.01)
        if mass[i] != 0:
            the_data = merge_other(the_data, snname, "mass", mass[i], 0.1)
            the_data = merge_other(the_data, snname, "mass+", mass_high[i] - mass[i], 0.1)
            the_data = merge_other(the_data, snname, "mass-", mass_low[i] - mass[i], 0.1)
        the_data = merge_other(the_data, snname, "SFD98", get_SFD98(the_data[snname]["RA"], the_data[snname]["Dec"]), 0.002)

    return the_data



def parse_PanSTARRS(the_data):
    [PS_SN, PS_RA, PS_Dec, NA, NA, zHost, NA, NA, NA, zSN, NA, NA, NA, NA, NA, NA, NA, NA, logM, e_logM] = readcol("PS_DR2/hlsp_ps1cosmo_panstarrs_gpc1_all_multi_v1_snparams.txt", 'a,aa,aa,fffa,f,ffffffff,ff')

    for lcfl in glob.glob("PS_DR2/*fits"):
        snname = lcfl.split("hlsp_ps1cosmo_ps1_gpc1_")[1].split("_g-r-i-z_")[0]
        PS_ind = PS_SN.index(snname)
        if zSN[PS_ind] > 0:

            if snname not in the_data:
                the_data[snname] = {}

            f = fits.open(lcfl)

            mjd = f[1].data["MJD"]
            flux = f[1].data["FLUXCAL"]
            fluxerr = f[1].data["FLUXCALERR"]
            mag = f[1].data["MAG"]
            band = f[1].data["FLT"]


            for i in range(len(mjd)):

                #if flux[i]/fluxerr[i] > 5:
                #    assert abs(mag[i] - (27.5 - 2.5*log10(flux[i]))) < 0.001, "Wrong ZP! %f %f" % (flux[i], mag[i])


                the_data[snname] = append_phot(the_data[snname], set_name = "Pan-STARRS", instrument = "PS", filt = "PS_" + band[i],
                                               magsys = "AB", mjd = mjd[i], flux = flux[i], dflux = fluxerr[i], zeropoint = 27.5)


            the_data = merge_other(the_data, snname, "z_heliocentric", f[0].header["Z_FINAL"], 0.003)
            the_data = merge_other(the_data, snname, "RA", f[0].header["RA_TARG"], 0.01)
            the_data = merge_other(the_data, snname, "Dec", f[0].header["Dec_TARG"], 0.01)
            the_data = merge_other(the_data, snname, "SFD98", get_SFD98(the_data[snname]["RA"], the_data[snname]["Dec"]), 0.002)
            the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(the_data[snname]["RA"], the_data[snname]["Dec"], the_data[snname]["z_heliocentric"]), 0.003)

            try:
                HGLOGM = f[0].header["HGLOGM"]
                HGLOGMER = f[0].header["HGLOGMER"]
            except:
                print("Didn't find mass, setting low")
                HGLOGM = 0.
                HGLOGMER = 1.

            the_data = merge_other(the_data, snname, "mass", HGLOGM, 0.1)
            the_data = merge_other(the_data, snname, "mass+", HGLOGMER, 0.1)
            the_data = merge_other(the_data, snname, "mass-", -HGLOGMER, 0.1)
            f.close()

    return the_data


def parse_See_Change(the_data):
    bad_sne = ['SCP15B02', 'SCP15E05', 'SCP16B02', 'SCP16H01']


    for lcfl in glob.glob("See-Change/v1/SCP1*t"):
        snname = lcfl.split("/")[-1].split(".txt")[0]

        if bad_sne.count(snname) == 0:
            if snname not in the_data:
                the_data[snname] = {}

            [mjd, band, flux, fluxerr, zerop, magsys] = readcol(lcfl, 'fafffa')
            inds = argsort(mjd)

            assert all(array(magsys) == "ab"), "magsys not ab" + lcfl

            redshift = read_param(lcfl, "@Redshift")
            ra = read_param(lcfl, "@RA")
            dec = read_param(lcfl, "@Dec")


            for i in inds:
                the_data[snname] = append_phot(the_data[snname], set_name = "See-Change", instrument = "WFC3", filt = "WFC3_" + band[i],
                                               magsys = "AB", mjd = mjd[i], flux = flux[i], dflux = fluxerr[i], zeropoint = zerop[i])


            the_data = merge_other(the_data, snname, "z_heliocentric", redshift, 0.003)
            the_data = merge_other(the_data, snname, "z_CMB", get_zCMB(ra, dec, redshift), 0.003)
            the_data = merge_other(the_data, snname, "RA", ra, 0.01)
            the_data = merge_other(the_data, snname, "Dec", dec, 0.01)
            the_data = merge_other(the_data, snname, "SFD98", get_SFD98(the_data[snname]["RA"], the_data[snname]["Dec"]), 0.002)
    return the_data


def add_neill(the_data):
    host_to_sn_dict = {}
    
    f = open("Neill_Mass/apj324724t1_ascii.txt")
    lines = f.read().split('\n')
    f.close()

    for line in lines:
        parsed = line.split('\t')
        if len(parsed) > 2 and parsed[0].strip() != "":
            host_to_sn_dict[parsed[1]] = snname_to_standard_snname(parsed[0])

    f = open("Neill_Mass/apj324724t2_ascii.txt")
    lines = f.read().split('\n')
    f.close()

    host_to_mass_dict = {}

    for line in lines:
        parsed = line.split('\t')

        if len(parsed) > 2 and line.count(".") > 2:
            mass = float(parsed[6])
            mass_l = float(parsed[5])
            mass_h = float(parsed[7])
            host = parsed[0]
            
            host_to_sn_dict[host]  # Just checking!
            
            if host_to_sn_dict[host] in the_data:
                the_data = merge_other(the_data, host_to_sn_dict[host], "mass", mass, 1)
                the_data = merge_other(the_data, host_to_sn_dict[host], "mass+", mass_h - mass, 1)
                the_data = merge_other(the_data, host_to_sn_dict[host], "mass-", mass_l - mass, 1)
            else:
                print("Couldn't find LC data for SN ", host_to_sn_dict[host])


    return the_data

        

def parse_Knop03():
    do_it("mkdir Union3/Knop03")

    redshift_dict = {"SN1997ek": 0.863,
                     "SN1997eq": 0.538,
                     "SN1997ez": 0.778,
                     "SN1998as": 0.355,
                     "SN1998aw": 0.440,
                     "SN1998ax": 0.497,
                     "SN1998ay": 0.638,
                     "SN1998ba": 0.430,
                     "SN1998be": 0.644,
                     "SN1998bi": 0.740,
                     "SN2000fr": 0.543}


    for orig_lc2fl in glob.glob("Knop_Stuff/astronomy/papers/knop2003/data/[12]*.dat"):
        print(orig_lc2fl)

        f = open(orig_lc2fl)
        lines = f.read().split('\n')
        f.close()

        snname = "SN" + orig_lc2fl.split("/")[-1].split("-")[0]
        band = orig_lc2fl.split("-")[-1].split(".")[0]
        zeropoint = read_param(orig_lc2fl, "zeropoint:")
        ndata = read_param(orig_lc2fl, "datapoints:")

        do_it("mkdir -p Union3/Knop03/" + snname)
        
        lc2fl = "Union3/Knop03/" + snname + "/lc2fit_Knop_" + band + ".dat"
        
        flc2 = open(lc2fl, 'w')
        flc2.write("""@INSTRUMENT Knop
@BAND Bessell12_""" + band + """
@MAGSYS """ + opts["Knop"]["BVRIgriMagsys"] + """
@WEIGHTMAT weightmat_Knop_""" + band + """.dat
#Date : 
#Flux : 
#Fluxerr :
#ZP : 
#end
""")


        covmat = array([], dtype=float64)

        for line in lines:
            parsed = line.split(None)
            if len(parsed) == 3 and is_number(parsed[0]):
                flc2.write(str(float(parsed[0]) + 2400000 - 2400000.5) + # SIGH!
                           "  " + parsed[1] + "  " + parsed[2] + "  " + str(zeropoint) + '\n')
            elif len(parsed) == 1 and is_number(parsed[0]):
                covmat = append(covmat, float(parsed[0]))
        assert len(covmat) == ndata**2, "Covmat wrong size! %i %i" % (ndata, len(covmat))
        
        covmat = reshape(covmat, [ndata, ndata])

        weightmat = linalg.inv(covmat)

        fweight = open("Union3/Knop03/" + snname + "/weightmat_Knop_" + band + ".dat", 'w')
        fweight.write("%i %i\n" % (ndata, ndata))

        for i in range(ndata):
            for j in range(ndata):
                fweight.write(str(weightmat[i,j]) + "  ")
            fweight.write('\n')

    [SN_name, MJD, NA, NA, flux, NA, dflux1, NA, dflux2] = readcol("Knop_Stuff/Nobili_Table1", 'afff,fafaf')
    SN_name = np.array(SN_name)
    for unique_name in np.unique(SN_name):
        lc2fl = "Union3/Knop03/" + unique_name + "/lc2fit_NICMOS2.dat"

        flc2 = open(lc2fl, 'w')
        flc2.write("""@INSTRUMENT NICMOS2
@BAND F110W_faint
@MAGSYS VEGAHST
#Date : 
#Flux : 
#Fluxerr :
#ZP : 
#end
""")
        
        inds = np.where(unique_name == SN_name)[0]
        for ind in inds:
            towrite = [MJD[ind], flux[ind], np.sqrt(dflux1[ind]**2. + dflux2[ind]**2.), 22.268]
            towrite = [str(item) for item in towrite]
            flc2.write("  ".join(towrite) + '\n')
        flc2.close()
    

    for snname in redshift_dict:

        RA, Dec = get_RA_Dec(snname)
        z_CMB = get_zCMB(RA, Dec, redshift_dict[snname])
        SFD98 = get_SFD98(RA, Dec)

        f = open("Union3/Knop03/" + snname + "/lightfile", 'w')
        f.write("z_heliocentric  " + str(redshift_dict[snname]) + '\n')
        f.write("z_cmb  " + str(z_CMB) + '\n')
        f.write("RA  " + str(RA) + '\n')
        f.write("Dec  " + str(Dec) + '\n')
        f.write("MWEBV  " + str(SFD98) + '\n')
        f.close()


def parse_SNLS():
    do_it("mkdir Union3/SNLS")

    dAB = dict(g = -0.0069*0,
               r = 0.0147*0,
               i = 0.0185*0,
               z = 0.0034*0) # Synthethic AB minus observed, updated for 2021 CALSPEC

    for snls_fl in glob.glob("JLA/jla_light_curves/lc-0[2-9]D[1-4]*.list"):
        snname = snls_fl.split("/lc-")[1].split(".list")[0]
        do_it("mkdir Union3/SNLS/" + snname)

        [mjd, flux, fluxerr, ZP, instrument_filt, magsys] = readcol(snls_fl, 'ffff,aa')
        instrument_filt = array([item[-1] for item in instrument_filt])

        for filt in 'griz':
            inds = where(instrument_filt == filt)[0]
            if len(inds) > 0:
                assert len(inds) == inds[-1] - inds[0] + 1, "Non-consective filters found " + str(filt) + snname
                
                f = open("Union3/SNLS/" + snname + "/lc2fit_SNLS_" + filt + ".dat", 'w')
                f.write("@INSTRUMENT MEGACAMPSF\n")
                f.write("@BAND " + filt + "\n")
                f.write("@MAGSYS AB\n")
                f.write("@WEIGHTMAT weightmat_SNLS_" + filt + ".dat\n")
                f.write("@X_FOCAL_PLANE " + str(read_param(snls_fl, "@X_FOCAL_PLANE", ind = 1)) + "\n")
                f.write("@Y_FOCAL_PLANE " + str(read_param(snls_fl, "@Y_FOCAL_PLANE", ind = 1)) + "\n")
                f.write("#Date : MJD\n")
                f.write("#Flux : \n")
                f.write("#Fluxerr : errors (but look at the weight matrix)\n")
                f.write("#ZP : \n")
                f.write("#end\n")
                
                for ind in inds:
                    f.write(str(mjd[ind]) + "  " + str(flux[ind]) + "  " + str(fluxerr[ind]) + "  " + str(ZP[ind] + dAB[filt]) + '\n')
                f.close()


                f = open("JLA/jla_light_curves/covmat_lc-" + snname + ".dat")
                lines = f.read().split('\n')
                f.close()

                lines = [line.strip() for line in lines if line.strip() != ""]

                cmat_size = int(lines[0].split(None)[0])
                assert cmat_size == len(mjd), "cmat not the same size as lc2file! " + snname

                cmat = zeros([cmat_size]*2, dtype=float64)
                lines = lines[1:]
                assert len(lines) == len(mjd), "cmat file not the same size as lc2file! " + snname + " " + str(len(lines)) + " " + str(len(mjd))
                
                for i in range(cmat_size):
                    parsed = lines[i].split(None)
                    assert len(parsed) == len(mjd), "cmat line not the same size as lc2file! " + str(parsed)
                    
                    for j in range(len(parsed)):
                        cmat[i,j] = float(parsed[j])
                
                wmat = linalg.inv(cmat)

                f = open("Union3/SNLS/" + snname + "/weightmat_SNLS_" + filt + ".dat", 'w')
                f.write(str(len(inds)) + " " + str(len(inds)) + '\n')
                for i in inds:
                    for j in inds:
                        f.write(str(wmat[i,j]) + "  ")
                    f.write('\n')
                f.close()
                

        mwebv = read_param(snls_fl, "@MWEBV")
        z_helio = read_param(snls_fl, "@Z_HELIO")
        ra = read_param(snls_fl, "@RA")
        dec = read_param(snls_fl, "@DEC")
        z_cmb = get_zCMB(ra, dec, z_helio)

        mass = read_param("JLA/table1.dat.txt", snname, ind = 3)
        mass_err_plus = read_param("JLA/table1.dat.txt", snname, ind = 4)
        mass_err_minus = read_param("JLA/table1.dat.txt", snname, ind = 5)
        
        
        
        f = open("Union3/SNLS/" + snname + "/lightfile", 'w')
        
        f.write("z_heliocentric  " + str(z_helio) + '\n')
        f.write("z_cmb  " + str(z_cmb) + '\n')
        f.write("RA  " + str(ra) + '\n')
        f.write("Dec  " + str(dec) + '\n')
        f.write("MWEBV  " + str(mwebv) + '\n')


        if mass != None:
            f.write("Mass  %.2f  %.2f  %.2f\n" % (mass, mass_err_minus, mass_err_plus))
        else:
            f.write("Mass  %.2f  %.2f  %.2f\n" % (0., -1, 1))
            
        f.close()

def parse_ESSENCE():
    do_it("mkdir Union3/ESSENCE")

    for essence_fl in glob.glob("ESSENCE/lcs/*.dat"):
        [objid, NA, NA, mwebv, zhelio, zcmb_test, ra, dec] = readcol(essence_fl, 'a,ff,fff,aa')
        if len(zhelio) == 1:
            [objid, mwebv, zhelio, zcmb_test, ra, dec] = [item[0] for item in [objid, mwebv, zhelio, zcmb_test, ra, dec]]


            snname = essence_fl.split("/")[-1].split(".")[0]
            do_it("mkdir Union3/ESSENCE/" + snname)

            [NA, mjd, instrument_filt, flux, fluxerr] = readcol(essence_fl, 'afaff')
            instrument_filt = array([item.replace("4m", "") for item in instrument_filt])

            for filt in "RI":
                inds = where(instrument_filt == filt)[0]
                if len(inds) > 0:                
                    f = open("Union3/ESSENCE/" + snname + "/lc2fit_MOSAICII_" + filt + ".dat", 'w')
                    f.write("@INSTRUMENT MOSAICII\n")
                    f.write("@BAND MOSAICII_" + filt + "\n")
                    f.write("@MAGSYS Linear_AB11_off\n")
                    f.write("#Date : MJD\n")
                    f.write("#Flux : \n")
                    f.write("#Fluxerr : \n")
                    f.write("#ZP : \n")
                    f.write("#end\n")

                    for ind in inds:
                        f.write(str(mjd[ind]) + "  " + str(flux[ind]) + "  " + str(fluxerr[ind]) + "  " + str(25.) + '\n')
                    f.close()


            assert objid == snname

            ra = float(ephem.Equatorial(ra, dec).ra)*180./pi
            dec = float(ephem.Equatorial(ra, dec).dec)*180./pi

            z_cmb = get_zCMB(ra, dec, zhelio)
            assert abs(z_cmb - zcmb_test) < 0.001

            f = open("Union3/ESSENCE/" + snname + "/lightfile", 'w')

            f.write("z_heliocentric  " + str(zhelio) + '\n')
            f.write("z_cmb  " + str(z_cmb) + '\n')
            f.write("RA  " + str(ra) + '\n')
            f.write("Dec  " + str(dec) + '\n')
            f.write("MWEBV  " + str(mwebv) + '\n')
            f.write("Mass  %.2f  %.2f  %.2f\n" % (10, -5, 5))

            f.close()
        else:
            print("Skipping ", essence_fl, "no z!")

    [SN_list, JD, filt_list, mag, dmag] = readcol("ESSENCE/table2_unpack.txt", 'a,f,a,ff')
    filt_list = np.array(filt_list)
    SN_list = np.array(SN_list)
    
    for SN in np.unique(SN_list):
        if glob.glob("Union3/ESSENCE/" + SN) == []:
            print("Skipping ", SN)
        else:
            for filt in np.unique(filt_list):
                inds = np.where((SN_list == SN)*(filt_list == filt))[0]

                if len(inds) > 0:
                    if filt == "F110W":
                        filt = filt + "_faint"
                        instr = "NICMOS2"
                    else:
                        instr = "ACSWF"
                    
                    f = open("Union3/ESSENCE/" + SN + "/lc2fit_" + filt + ".dat", 'w')
                    f.write("@INSTRUMENT " + instr + "\n")
                    f.write("@BAND " + filt + "\n")
                    f.write("@MAGSYS VEGAHST\n")
                    f.write("#Date : MJD\n")
                    f.write("#Mag : \n")
                    f.write("#Magerr : \n")
                    f.write("#end\n")

                    for ind in inds:
                        f.write(str(JD[ind] - 2400000.5) + "  " + str(mag[ind]) + "  " + str(dmag[ind]) + '\n')
                    f.close()




def parse_DES3():
    """C1	03:37:05.83	-27:06:41.8	shallow
C2	03:37:05.83	-29:05:18.2	shallow
C3	03:30:35.62	-28:06:00.0	deep
E1	00:31:29.86	-43:00:34.6	shallow
E2	00:38:00.00	-43:59:52.8	shallow
S1	02:51:16.80	+00:00:00.0	shallow
S2	02:44:46.66	-00:59:18.2	shallow
X1	02:17:54.17	-04:55:46.2	shallow
X2	02:22:39.48	-06:24:43.6	shallow
X3	02:25:48.00	-04:36:00.0	deep
"""
    shallow_fields = ["C1", "C2", "E1", "E2", "S1", "S2", "X1", "X2"]
    deep_fields = ["C3", "X3"]
    
    do_it("mkdir Union3/DES3_Shallow")
    do_it("mkdir Union3/DES3_Deep")


    """AB old, new g 16.69454036597108 16.681835865668685 -0.01270450030239445
    AB old, new r 16.340211601143032 16.338558043742395 -0.0016535574006368847
    AB old, new i 16.257031013471163 16.257528172771323 0.0004971593001599217
    AB old, new z 16.24497926023369 16.243796081060914 -0.0011831791727772156
    AB old, new y 16.26724219685197 16.247995066840748 -0.019247130011223135
    """

    add_to_mags = dict(g = -0.01270450030239445, r = -0.0016535574006368847, i = 0.0004971593001599217, z = -0.0011831791727772156, y = -0.019247130011223135)
    
    for des_fl in glob.glob("DES3/02-DATA_PHOTOMETRY/DES-SN3YR_DES/*dat"):
        snname = des_fl.split("/des_")[1].split(".dat")[0]

        [NA, mjd, instrument_filt, des_field, flux, fluxerr, ZP] = readcol(des_fl, 'afaafff')

        if shallow_fields.count(des_field[0]):
            wd = "Union3/DES3_Shallow/" + snname
        elif deep_fields.count(des_field[0]):
            wd = "Union3/DES3_Deep/" + snname
        else:
            assert 0
        
        do_it("mkdir " + wd)
        
        instrument_filt = array(instrument_filt)

        for filt in np.unique(instrument_filt):
            inds = where(instrument_filt == filt)[0]
            if len(inds) > 0:                
                f = open(wd + "/lc2fit_DES_" + filt + ".dat", 'w')
                f.write("@INSTRUMENT DECam\n")
                f.write("@BAND DECam_" + filt + "\n")
                f.write("@MAGSYS AB\n")
                f.write("#Date : MJD\n")
                f.write("#Flux : \n")
                f.write("#Fluxerr : \n")
                f.write("#ZP : \n")
                f.write("#end\n")
                
                for ind in inds:
                    f.write(str(mjd[ind]) + "  " + str(flux[ind]) + "  " + str(fluxerr[ind]) + "  " + str(27.5 + add_to_mags[filt]) + "\n")  # + str(ZP[ind]) + '\n')
                f.close()

        ra = read_param(des_fl, "RA:")
        dec = read_param(des_fl, "DECL:")
        mwebv = read_param(des_fl, "MWEBV:")
        z_helio = read_param(des_fl, "REDSHIFT_HELIO:")
        z_cmb = get_zCMB(ra, dec, z_helio)

        mass = read_param(des_fl, "HOSTGAL_LOGMASS:", ind = 1)
        assert read_param(des_fl, "HOSTGAL_LOGMASS:", ind = 2) == "+-"
        mass_err_plus = read_param(des_fl, "HOSTGAL_LOGMASS:", ind = 3)
        mass_err_minus = -mass_err_plus

                
        f = open(wd + "/lightfile", 'w')
        
        f.write("z_heliocentric  " + str(z_helio) + '\n')
        f.write("z_cmb  " + str(z_cmb) + '\n')
        f.write("RA  " + str(ra) + '\n')
        f.write("Dec  " + str(dec) + '\n')
        f.write("MWEBV  " + str(mwebv) + '\n')
        f.write("Mass  %.2f  %.2f  %.2f\n" % (mass, mass_err_minus, mass_err_plus))
            
        f.close()

def match_RAISIN_to_DES(RAISIN_SN, RA, Dec, zhelio):
    """Sigh"""
    matches = 0
    closest = 100

    print("Matching ", RAISIN_SN, RA, Dec, zhelio)

    for DES_lfl in glob.glob("Union3/DES3*/*/lightfile"):
        this_RA = read_param(DES_lfl, "RA")
        this_Dec = read_param(DES_lfl, "Dec")
        this_zhelio = read_param(DES_lfl, "z_heliocentric")

        closest = min(closest, np.sqrt(
            (this_RA - RA)**2. + 
            (this_Dec - Dec)**2. + 
            (this_zhelio - zhelio)**2.))
        
        if np.abs(this_RA - RA) < 0.002:
            if np.abs(this_Dec - Dec) < 0.002:
                if np.abs(zhelio - this_zhelio) < 0.01:
                    matches += 1

                    return_dir = DES_lfl.replace("lightfile", "")
    if matches != 1:
        print("Couldn't find RAISIN_SN " + RAISIN_SN + " closest " + str(closest))
        return ""
    
    return return_dir


def parse_RAISIN():
    [SNe, MJD, filt, mag, NA, dmag] = readcol("RAISIN/apjac755bt6_ascii.txt", 'afafaf')
    SNe_filt = np.array(
        [SNe[i] + ":" + filt[i] for i in range(len(SNe))]
        )


    [SN_list, RA_list, Dec_list, z_helio_list] = readcol("RAISIN/apjac755bt5_ascii.txt", 'aaaf')

    RA_dict = {}
    Dec_dict = {}
    zhelio_dict = {}

    for i in range(len(SN_list)):
        RA = float(ephem.Equatorial(RA_list[i], Dec_list[i]).ra)*180./pi
        Dec = float(ephem.Equatorial(RA_list[i], Dec_list[i]).dec)*180./pi

        RA_dict[SN_list[i]] = RA
        Dec_dict[SN_list[i]] = Dec
        zhelio_dict[SN_list[i]] = z_helio_list[i]
        
    
    for SN_filt in set(SNe_filt):
        
        inds = np.where(SNe_filt == SN_filt)

        tmp_SN_name = SN_filt.split(":")[0]
        tmp_filt_name = SN_filt.split(":")[1]
        
        if tmp_SN_name.count("PS1-"):
            wd = "Union3/Pan-STARRS/" + tmp_SN_name.replace("PS1-", "psc") + "/"
        else:
            wd = match_RAISIN_to_DES(tmp_SN_name, RA_dict[tmp_SN_name], Dec_dict[tmp_SN_name], zhelio_dict[tmp_SN_name])
        

        if glob.glob(wd + "lightfile") == []:
            print(wd, "not found!")
        else:

            f = open(wd + "/lc2fit_" + tmp_filt_name + ".dat", 'w')
            f.write("@INSTRUMENT  WFC3\n")
            f.write("@BAND " + "WFC3_" + tmp_filt_name.lower() + "\n")
            f.write("@MAGSYS AB \n")
            f.write("#Date : \n")
            f.write("#Mag : \n")
            f.write("#Magerr : \n")
                        
            for ind in inds[0]:
                assert tmp_SN_name == SNe[ind]
                assert tmp_filt_name == filt[ind]

                f.write(str(MJD[ind]) + "  " + str(mag[ind]) + "  " + str(dmag[ind]) + "\n")
                
            f.close()

    
                
def parse_MCT():
    #CLA10Cal  55579.348     F125W   J       0.805   0.637   >26.8   cdots   
    [SNe, mjds, filts, NA, fluxes, dfluxes] = readcol("MCT/apjaaa5a9t8_ascii.txt", 'afaaff')
    SNe = np.array(SNe)
    filts = np.array(filts)

    [SN_names, SN_redshifts] = readcol("MCT/apjaaa5a9t2_ascii.txt", 'af')
    [SN_coords_names, NA, NA, NA, RAhms, Decdms] = readcol("MCT/Table1.txt", 'aaaaaa')
    

    for SN in np.unique(SNe):
        ind = SN_names.index(SN)
        subprocess.getoutput("mkdir -p Union3/MCT/" + SN)


        coords_ind = SN_coords_names.index(SN)
        
        RA = float(ephem.Equatorial(RAhms[coords_ind], Decdms[coords_ind]).ra)*180./pi
        Dec = float(ephem.Equatorial(RAhms[coords_ind], Decdms[coords_ind]).dec)*180./pi

        
        f = open("Union3/MCT/" + SN + "/lightfile", 'w')
        
        f.write("z_heliocentric  " + str(SN_redshifts[ind]) + '\n')
        f.write("z_cmb  " + str(SN_redshifts[ind]) + '\n')
        f.write("RA  " + str(RA) + '\n')
        f.write("Dec  " + str(Dec) + '\n')
        f.write("MWEBV  " + str(get_SFD98(RA, Dec)) + '\n')
        f.write("Mass  %.2f  %.2f  %.2f\n" % (10., -5., 5.))
        print("Mass HACK!!!!"*100)

        f.close()

        inds = np.where(SNe == SN)

        for band in np.unique(filts[inds]):
            inds = np.where((SNe == SN)*(filts == band))
            print("Possible hack ACS/WFC3!!!!")
            instrument = {
                "F606W": "ACSWF",
                "F775W": "ACSWF",
                "F814W": "ACSWF",
                "F850LP": "ACSWF",
                "F105W": "WFC3",
                "F125W": "WFC3",
                "F127M": "WFC3",
                "F139M": "WFC3",
                "F140W": "WFC3",
                "F153M": "WFC3",
                "F160W": "WFC3"}[band]

            AB_offset_to_add = dict(F125W = -0.0048, F127M = -0.0050, F139M = -0.0048,
                                    F140W = -0.0050, F153M = -0.0048, F160W = -0.0051,
                                    F606W = -0.0093, F775W = -0.0071, F814W = -0.0066, F850LP = -0.0051)[band]
            
                          
            
            f = open("Union3/MCT/" + SN + "/lc2fit_" + band + ".dat", 'w')
            f.write("@INSTRUMENT " + instrument + "\n")
            f.write("@BAND " + band*int(instrument == "ACSWF") + ("WFC3_" + band.lower())*int(instrument == "WFC3") + "\n")
            f.write("@MAGSYS AB\n")
            f.write("#Date : \n")
            f.write("#Flux : \n")
            f.write("#Fluxerr : \n")
            f.write("#ZP : \n")
                        
            for ind in inds[0]:
                f.write(str(mjds[ind]) + "  " + str(fluxes[ind]) + "  " + str(dfluxes[ind]) + "  " + str(27.5 + AB_offset_to_add) + "\n")
            f.close()


def parse_Suzuki12(the_data):
    [SNe, NA, zSN, NA, RAhms, Decdms] = readcol("Suzuki12/apj412614t1_ascii.txt", 'aaffaa')
    [SN_LC, instr, filt, mjd, flux, dflux, vega_zp, NA, NA, flux3, dflux3] = readcol("Suzuki12/apj412614t2_mrt.txt", 'aaa,ffff,fi,ff')
    SN_LC = np.array(SN_LC)
    instr = np.array(instr)
    filt = np.array(filt)
    
    for SN_ind, SN in enumerate(SNe):
        inds = np.where(SN_LC == SN)[0]

        if len(inds) > 2:
            if SN not in the_data:
                the_data[SN] = {}

                
            for i in inds:
                if filt[i] == "F775W":
                    this_instrument = "ACSWF"
                    this_filt = "F775W"
                    if mjd[i] < 53931:
                        this_zeropoint = 25.291
                    else:
                        this_zeropoint = 25.277
                    this_magsys = "VEGAHST"

                elif filt[i] == "F850LP":
                    this_instrument = "ACSWF"
                    this_filt = "F850LP_3pix"
                    this_magsys = "VEGAHST"
                    if mjd[i] < 53931:
                        this_zeropoint = 24.347 - 0.438
                    else:
                        this_zeropoint = 24.323 - 0.438
                    flux[i] = flux3[i]
                    dflux[i] = dflux3[i]

                elif filt[i] == "F110W":
                    this_instrument = "NICMOS2"
                    this_filt = "F110W_faint"
                    this_zeropoint = 25.296
                    this_magsys = "ST"
                elif filt[i] == "H":
                    this_instrument = "KeckAO"
                    this_filt = "Keck_H"
                    this_zeropoint = vega_zp[i]
                    this_magsys = "VEGAHST"
                else:
                    assert 0

                the_data[SN] = append_phot(the_data[SN], set_name = "SuzukiRubin", instrument = this_instrument, filt = this_filt,
                                           magsys = this_magsys, mjd = mjd[i], flux = flux[i], dflux = dflux[i], zeropoint = this_zeropoint)


            the_data = merge_other(the_data, SN, "z_heliocentric", zSN[SN_ind], 0.003)
            RA = float(ephem.Equatorial(RAhms[SN_ind], Decdms[SN_ind]).ra)*180./pi
            Dec = float(ephem.Equatorial(RAhms[SN_ind], Decdms[SN_ind]).dec)*180./pi


            the_data = merge_other(the_data, SN, "z_CMB", get_zCMB(RA, Dec, zSN[SN_ind]), 0.003)
            the_data = merge_other(the_data, SN, "RA", RA, 0.01)
            the_data = merge_other(the_data, SN, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, SN, "SFD98", get_SFD98(the_data[SN]["RA"], the_data[SN]["Dec"]), 0.002)

    do_it("mkdir -p Union3/SuzukiRubin")
    do_it("cp -r Suzuki12/Mingus Union3/SuzukiRubin")
            
    return the_data





def parse_Riess07(the_data):
    [SNe, NA, RAhms, Decdms] = readcol("Riess07/Table1", 'aaaa')
    [SN_LC, redshift, JD, filt, mag, dmag] = readcol("Riess07/datafile2.txt", 'affaff')

    JD += 2400000.0
    mjd = JD - 2400000.5

    SN_LC = np.array(SN_LC)
    filt = np.array(filt)
    
    for SN_ind, SN in enumerate(SNe):
        inds = np.where(SN_LC == SN[:8])[0]
        
        if len(inds) > 2:
            if SN not in the_data:
                the_data[SN] = {}

                
            for i in inds:
                if filt[i] == "F775W":
                    this_instrument = "ACSWF"
                    this_filt = "F775W"
                    this_magsys = "VEGAHST"
                    this_zeropoint = 25.28
                    
                elif filt[i] == "F850LP":
                    this_instrument = "ACSWF"
                    this_filt = "F850LP"
                    this_magsys = "VEGAHST"
                    this_zeropoint = 24.35
                        
                elif filt[i] == "F606W":
                    this_instrument = "ACSWF"
                    this_filt = "F606W"
                    this_magsys = "VEGAHST"
                    this_zeropoint = 26.43

                elif filt[i] == "F110W":
                    this_instrument = "NICMOS2"
                    this_filt = "F110W_faint"
                    this_zeropoint = 22.92
                    this_magsys = "VEGAHST"
                    
                elif filt[i] == "F160W":
                    this_instrument = "NICMOS2"
                    this_filt = "F160W"
                    this_zeropoint = 22.11
                    this_magsys = "VEGAHST"
                else:
                    assert 0


                if mag[i] > 10:
                    this_flux = 10.**(0.4*(this_zeropoint - mag[i]))
                    this_dflux = 0.92103*this_flux*dmag[i]
                else:
                    this_flux = mag[i]
                    this_dflux = dmag[i]
                    
                the_data[SN] = append_phot(the_data[SN], set_name = "Riess07", instrument = this_instrument, filt = this_filt,
                                           magsys = this_magsys, mjd = mjd[i], flux = this_flux, dflux = this_dflux, zeropoint = this_zeropoint)

            assert (redshift[inds[0]] == redshift[inds]).all()
            the_data = merge_other(the_data, SN, "z_heliocentric", redshift[inds[0]], 0.003)
            RA = float(ephem.Equatorial(RAhms[SN_ind], Decdms[SN_ind]).ra)*180./pi
            Dec = float(ephem.Equatorial(RAhms[SN_ind], Decdms[SN_ind]).dec)*180./pi

            the_data = merge_other(the_data, SN, "z_CMB", get_zCMB(RA, Dec, the_data[SN]["z_heliocentric"]), 0.003)
            the_data = merge_other(the_data, SN, "RA", RA, 0.01)
            the_data = merge_other(the_data, SN, "Dec", Dec, 0.01)
            the_data = merge_other(the_data, SN, "mass", 10., 0.1)
            the_data = merge_other(the_data, SN, "mass+", 5., 0.1)
            the_data = merge_other(the_data, SN, "mass-", -5., 0.1)
            the_data = merge_other(the_data, SN, "SFD98", get_SFD98(the_data[SN]["RA"], the_data[SN]["Dec"]), 0.002)
        else:
            assert 0, SN + " " + str(len(inds))

            
    return the_data






def parse_Tonry03():
    do_it("cp -r tmp_union3/tonryetal Union3/Tonry03")

    for lc2fl in glob.glob("Union3/Tonry03/*/lc2fit*dat"):
        f = open(lc2fl, 'r')
        lines = f.read().split('\n')
        f.close()

        band = ""
        for i in range(len(lines)):
            if lines[i].count("BAND"):
                band = lines[i].split(None)[1]
            if lines[i].count("MAGSYS"):
                if band == "TONRY_J" or band == "TONRY_Z":
                    lines[i] = "@MAGSYS VEGAHST"
                else:
                    lines[i] = "@MAGSYS Linear_AB11_off"
        f = open(lc2fl, 'w')
        f.write('\n'.join(lines))
        f.close()

    do_it("rm -fv Union3/Tonry03/*/result*")

def parse_Amanullah10():
    do_it("mkdir Union3/Amanullah10")
    do_it("cp -r tmp_union3/amanullahetal/2001* Union3/Amanullah10/")

    for lc2fl in glob.glob("Union3/Amanullah10/*/lc2fit*dat"):
        f = open(lc2fl, 'r')
        lines = f.read().split('\n')
        f.close()

        for i in range(len(lines)):
            if lines[i].count("MAGSYS"):
                print(lines[i].split(None))
                if lines[i].split(None)[1] == "VEGA":
                    lines[i] = "@MAGSYS  Linear_AB11_off"
        f = open(lc2fl, 'w')
        f.write('\n'.join(lines))
        f.close()

    do_it("rm -fv Union3/Amanullah10/*/result*")

                

def clear_out_files_to_ignore():
    patterns_to_ignore = []

    for pattern in patterns_to_ignore:
        files = glob.glob("Union3/*/*/lc2fit*" + pattern + "*.dat")
        for fl in files:
            print("Ignoring ", fl)
            newfl = fl.replace("lc2fit_", "ignore_lc2fit_")
            assert newfl != fl, "Problem with file! "  + fl
            print(subprocess.getoutput("mv -v " + fl + " " + newfl))


def match_list_of_hosts(sn):
    z_helio = -1
    z_helio_unc = 1
    
    for gal_key in list_of_hosts:
        if list_of_hosts[gal_key]["SNe"].count(sn):
            z_helio = list_of_hosts[gal_key]["zhelio"][0]
            z_helio_unc = list_of_hosts[gal_key]["zhelio"][1]

    return z_helio, z_helio_unc


################################# Main ################################

if __name__ == "__main__":
    # Note: zeropoint == -1 => magnitudes, not flux

    opts = read_paramfile(sys.argv[1])

    do_it("rm -fr Union3")
    do_it("mkdir Union3")

    #f = open("NED_info_SFD98.txt", 'a') # Create if it doesn't exist already
    #f.close()

    f = open("NED_info_SNRADec.txt", 'a') # Create if it doesn't exist already
    f.close()

    f = open("list_of_hosts.json", 'r')
    list_of_hosts = json.loads(f.read())
    f.close()

    SFD = sfdmap.SFDMap("/Users/" + whoami + "/Dropbox/SCP_Stuff/SFD98/")

    
    the_data = {}
    if opts["NB99"]["Include"]:
        the_data = parse_NB99(the_data)

    if opts["CalanTololo"]["Include"]:
        the_data = parse_CalanTololo(the_data)

    if opts["Krisciunas"]["Include"]:
        the_data = parse_krisciunas(the_data)

    if opts["CfA1"]["Include"]:
        the_data = parse_CfA1(the_data)

    if opts["CfA2"]["Include"]:
        the_data = parse_CfA2(the_data)

    if opts["CfA3"]["Include"]:
        the_data = parse_CfA3(the_data)

    if opts["CfA4"]["Include"]:
        the_data = parse_CfA4(the_data)

    assert (list(opts.keys()).count("CSP") == 0) or (list(opts.keys()).count("CSP3") == 0)

    if "CSP" in opts:
        if opts["CSP"]["Include"]:
            the_data = parse_CSP(the_data)
    if "CSP3" in opts:
        if opts["CSP3"]["Include"]:
            the_data = parse_CSP3(the_data)

    if opts["Foundation"]["Include"]:
        the_data = parse_Foundation(the_data)

    if opts["LSQ"]["Include"]:
        the_data = parse_LSQ(the_data)

    if opts["LSQ+LCO"]["Include"]:
        the_data = parse_LSQLCO(the_data)
        
    if opts["CNIa02"]["Include"]:
        the_data = parse_CNIa02(the_data)

    if opts["SDSS"]["Include"]:
        the_data = parse_SDSS(the_data)

    if opts["PanSTARRS"]["Include"]:
        the_data = parse_PanSTARRS(the_data)

    if opts["See-Change"]["Include"]:
        the_data = parse_See_Change(the_data)

    if opts["LOSS"]["Include"]:
        the_data = parse_LOSS(the_data)
        the_data = parse_LOSS2(the_data)

    if opts["MCT"]["Include"]:
        parse_MCT()

    if opts["Suzuki12"]["Include"]:
        the_data = parse_Suzuki12(the_data)

    if opts["Riess07"]["Include"]:
        the_data = parse_Riess07(the_data)

    if opts["Tonry03"]["Include"]:
        parse_Tonry03()

    if opts["Amanullah10"]["Include"]:
        parse_Amanullah10()

    the_data = add_neill(the_data) # This should be run last!

    if opts["SNLS"]["Include"]:
        parse_SNLS()

    if opts["ESSENCE"]["Include"]:
        parse_ESSENCE()

    if opts["DES3"]["Include"]:
        parse_DES3()

    if opts["DES3"]["Include"] or opts["PanSTARRS"]["Include"]:
        parse_RAISIN()

    if opts["Knop"]["Include"]:
        parse_Knop03()


    #if opts["Other"]:
    #    copy_old()

    set_counts = {}

    fRADec = open("all_parsed_RA_Dec.txt", 'w')

    for key in the_data:
        print("Writing ", key)

        set_name = get_set_name(the_data[key])

        wd = "Union3/" + set_name + "/" + key
        do_it("mkdir -p " + wd)

        make_lc2_files(the_data[key], wd)

        try:
            set_counts[set_name] += 1
        except:
            set_counts[set_name] = 1
        fRADec.write("%s\t\t%f\t%f\n" % (key, the_data[key]["RA"], the_data[key]["Dec"]))

    fRADec.close()
    clear_out_files_to_ignore()

    for key in set_counts:
        print(key, set_counts[key])

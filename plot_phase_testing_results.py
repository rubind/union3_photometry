import numpy as np
import matplotlib.pyplot as plt
from FileRead import read_param
from Spectra import check_derivs
import pickle
from matplotlib.backends.backend_pdf import PdfPages


def plot_summary(delta_mu, summfn, title, regionfn = None):
    #First_03_Last_10_SNRmax_500

    all_x = []
    all_y = []
    all_summ = []

    
    for key in delta_mu:
        fp = float(key.split("_")[1])
        lp = float(key.split("_")[3])
        assert key.split("_")[2] == "Last"

        all_x.append(fp)
        all_y.append(lp)
        all_summ.append(summfn(delta_mu[key]))
        
            
    all_x = np.array(all_x)
    all_y = np.array(all_y)

    fig = plt.figure(figsize = (15, 5))

    plt.subplot(1,3,1)
    plt.scatter(all_x, all_y, c = all_summ, marker = 's', cmap = "magma", s = 140)
    if regionfn != None:
        for i in range(len(all_x)):
            if regionfn(all_x[i], all_y[i]):
                plt.plot(all_x[i], all_y[i], '.', color = (0, 1, 0), zorder = 1)
    
    plt.xlabel("First Phase")
    plt.ylabel("Last Phase")

    plt.subplot(1,3,2)
    plt.scatter(all_x, all_y - all_x, c = all_summ, marker = 's', cmap = "magma", s = 110)
    plt.xlabel("First Phase")
    plt.ylabel("Last Phase - First Phase")
    if regionfn != None:
        for i in range(len(all_x)):
            if regionfn(all_x[i], all_y[i]):
                plt.plot(all_x[i], all_y[i] - all_x[i], '.', color = (0, 1, 0), zorder = 1)

    plt.title(title)

    plt.subplot(1,3,3)
    plt.scatter(all_y, all_y - all_x, c = all_summ, marker = 's', cmap = "magma", s = 110)
    if regionfn != None:
        for i in range(len(all_x)):
            if regionfn(all_x[i], all_y[i]):
                plt.plot(all_y[i], all_y[i] - all_x[i], '.', color = (0, 1, 0), zorder = 1)
    plt.xlabel("Last Phase")
    plt.ylabel("Last Phase - First Phase")
    
    
    plt.colorbar()
    fig.tight_layout()
    pdf.savefig(fig)
    plt.close()

def clipped_mean(x):
    inds = np.where(np.abs(x) < 0.5)
    return np.mean(x[inds])

def clipped_pull(x):
    inds = np.where(np.abs(x) < 0.5)
    the_mean = np.mean(x[inds])
    the_std = np.std(x[inds], ddof=1)
    the_std /= np.sqrt(len(inds[0]))

    return the_mean/the_std

def get_NMAD(x):
    return 1.4826*np.median(np.abs(x - np.median(x)))

def strong_cut(fp, lp):
    return (lp >= 8)*(fp <= 2)*(lp - fp >= 10) or (lp >= 8)*(fp <= 6)*(lp - fp >= 15)

delta_mu = pickle.load(open("delta_mu.pickle", 'rb'))

pdf = PdfPages("phase_testing.pdf")

plot_summary(delta_mu, lambda x: np.median(x), "Median")
plot_summary(delta_mu, clipped_mean, "Clipped Mean", regionfn = strong_cut)
plot_summary(delta_mu, lambda x: (get_NMAD(x) < 0.1)*2 + (np.abs(clipped_mean(x)) < 0.03), "NMAD < 0.1, |Clipped Mean| < 0.03", regionfn = strong_cut)
plot_summary(delta_mu, lambda x: (get_NMAD(x) < 0.15)*2 + (np.abs(clipped_mean(x)) < 0.05), "NMAD < 0.15, |Clipped Mean| < 0.05")
plot_summary(delta_mu, clipped_pull, "Clipped Pull")
plot_summary(delta_mu, lambda x: np.abs(clipped_pull(x)) < 2, "|Clipped Pull| < 2")
plot_summary(delta_mu, get_NMAD, "NMAD", regionfn = strong_cut)
plot_summary(delta_mu, lambda x: sum(np.abs(x) > 0.05)/float(len(x)), "> 0.05")
plot_summary(delta_mu, lambda x: sum(np.abs(x) > 0.1)/float(len(x)), "> 0.1")  
plot_summary(delta_mu, lambda x: sum(np.abs(x) > 0.1)/float(len(x)), "> 0.15")

max_good = 0
for key in delta_mu:
    max_good = max(max_good, len(delta_mu[key]))
                   
plot_summary(delta_mu, lambda x: len(x), "Successful Fits (max = %i)" % max_good)
pdf.close()

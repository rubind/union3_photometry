from numpy import *
from FileRead import readcol, writecol

lamb, u, g, r, i, z, atm = readcol("aj330387t4_ascii.txt", 'f,fffff,f')

filts = {"u": u, "g": g, "r": r, "i": i, "z": z}

for key in filts:
    inds = where(filts[key]*atm != 0)[0]
    

    writecol("SDSS_" + key + ".dat", [lamb[inds[0]-1:inds[-1]+2], filts[key][inds[0]-1:inds[-1]+2]   * atm [inds[0]-1:inds[-1]+2]  ])


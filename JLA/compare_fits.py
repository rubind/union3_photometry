import glob
from FileRead import read_param, readcol
import matplotlib.pyplot as plt
from numpy import *

[NA, NA, zhelio, NA, mB, NA, x1, NA, color] = readcol("jla_lcparams.txt", 'afff,ff,ff,f')

for resfl in glob.glob("../Union3/SDSS/*/result_salt2.dat"):
    res_z = read_param(resfl, "Redshift")
    res_mB = read_param(resfl, "RestFrameMag_0_B")
    res_c = read_param(resfl, "Color")
    res_x1 = read_param(resfl, "X1")
    
    inds = where(abs(zhelio - res_z) < 1.e-3)

    if len(inds[0]) > 0:
        matched_mB = mB[inds]
        matched_c = color[inds]
        matched_x1 = x1[inds]
        best_ind = argmin(abs(matched_mB - res_mB))
        
        matched_mB = matched_mB[best_ind]
        matched_c = matched_c[best_ind]
        matched_x1 = matched_x1[best_ind]
        
        plt.subplot(4,1,1)
        plt.plot(res_z, matched_mB - res_mB, '.', color = 'k')
        plt.ylim(-0.04, 0.04)
        plt.title("mB")

        plt.subplot(4,1,2)
        plt.plot(res_z, matched_c - res_c, '.', color = 'k')
        plt.ylim(-0.04, 0.04)
        plt.title("c")

        plt.subplot(4,1,3)
        plt.plot(res_z, matched_x1 - res_x1, '.', color = 'k')
        plt.ylim(-0.04, 0.04)
        plt.title("x1")

        plt.subplot(4,1,4)
        plt.plot(res_z, (matched_mB - res_mB) - 3*(matched_c - res_c) + 0.13*(matched_x1 - res_x1), '.', color = 'k')
        plt.ylim(-0.04, 0.04)
        plt.title("mu")


for i in range(1,5):
    plt.subplot(4,1,i)
    xlim = plt.xlim()
    plt.plot(xlim, [0]*2, color = 'cyan')
    plt.xlim(xlim)

    
plt.show()

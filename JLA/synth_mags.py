from numpy import *
from scipy.interpolate import interp1d
from FileRead import readcol

def ifl(fl):
    [x, y] = readcol(fl, 'ff')
    return interp1d(x, y, kind = 'linear', fill_value = 0, bounds_error = False)


lambs = arange(2000., 12000.)
bd17fn = ifl("../../calspec/bd_17d4708_stisnic_006.ascii")
abmag = lambda x: 0.108848/x**2.


for filt in 'ugriz':
    filtfn = ifl("SDSS_" + filt + ".dat")
    
    print filt, -2.5*log10(   sum(lambs*bd17fn(lambs)*filtfn(lambs))/
                              sum(lambs*abmag(lambs)*filtfn(lambs))   )

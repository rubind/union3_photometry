from FileRead import readcol, writecol
import glob


[cid, peak_ra, peak_decl, nepoch, official_name, SN_type] = readcol("sdsssn_master.dat2.txt", 'affiaa')

jla_lc = glob.glob("jla_light_curves/lc-SDSS*.list")

jla_lc = [int(item.split("lc-SDSS")[1].split(".list")[0]) for item in jla_lc]


print len(jla_lc)

paths = []

for item in jla_lc:
    count = 0
    for i in range(len(cid)):
        if str(item) == cid[i]:
            count += 1
            if SN_type[i] == "SNIa":
                if len(official_name[i]) > 3:
                    paths.append("/home/rubind/marg_test/sndata/Union3/SDSS/SN" + official_name[i] + "/")
                else:
                    paths.append("/home/rubind/marg_test/sndata/Union3/SDSS/SN-SDSS" + cid[i] + "/")
    assert count == 1, "Couldn't find " + str(item)

writecol("SDSS_JLA.txt", [paths])



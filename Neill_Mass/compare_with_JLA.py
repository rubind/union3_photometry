import glob
from numpy import *
from FileRead import readcol, read_param
import matplotlib.pyplot as plt
from string import upper
import commands

[jlasn, NA, NA, NA, NA,
 NA, NA, NA,
 NA, NA, jlamass] = readcol("../JLA/jla_lcparams.txt", 'a,fff,ff,ff,ff,f')
jlasn = [upper(item) for item in jlasn]

comps = []

not_in_jla = []
for lfl in glob.glob("../Union3/*/*/lightfile"):
    mass = read_param(lfl, "Mass")

    snname = upper(lfl.split("/")[-2])

    if jlasn.count(snname):
        if mass > 1:
            ind = jlasn.index(snname)
            comps.append(mass - jlamass[ind])
            
            if abs(comps[-1]) > 1:
                print snname, mass - jlamass[ind], mass, jlamass[ind]
    else:
        if lfl.count("amanullah10"):
            pass
        else:
            not_in_jla.append(snname)

print "not_in_jla", not_in_jla


for sn in jlasn:
    if len(glob.glob("../Union3/*/" + sn + "/lightfile")) != 1:
        print "Didn't find ", sn


#plt.hist(comps, bins = 40)
#plt.show()




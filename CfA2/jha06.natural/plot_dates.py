from numpy import *
import pylab
import glob

f = open("../datafile4.txt")
lines = f.read().split('\n')
f.close()


dates_by_filt = {}
for line in lines:
    parsed = line.split(None)
    if len(parsed) > 4 and parsed[0] == "SN":
        if dates_by_filt.has_key(parsed[-1]):
            dates_by_filt[parsed[-1]].append(float(parsed[2]))
        else:
            dates_by_filt[parsed[-1]] = [float(parsed[2])]

keys = dates_by_filt.keys()
dates_keys = [[mean(dates_by_filt[key]), key] for key in keys]
dates_keys.sort()
keys = [item[1] for item in dates_keys]

for key in keys:
    print key, min(dates_by_filt[key]) - 2400000.5, max(dates_by_filt[key]) - 2400000.5


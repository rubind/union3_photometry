from numpy import *
from FileRead import readcol, writecol
from Spectra import file_to_function


[band, nm, andysao, andyharris, foursao, fourharris] = readcol("datafile5.txt", 'af,ffff')
band = array(band)
atm_fn = file_to_function("kpnoextinct.txt")


for filt in "UBVRI":
    inds = where(band == filt)

    if filt == "U":
      atm = 1.
    else:
        atm = 10.**(-0.4*atm_fn(nm[inds]*10.))

    writecol("CfA2_AndyCamSAO_" + filt + ".txt", [nm[inds]*10., andysao[inds]*500/nm[inds]*atm])
    writecol("CfA2_AndyCamHarris_" + filt + ".txt", [nm[inds]*10., andyharris[inds]*500/nm[inds]*atm])
    writecol("CfA2_4ShooterSAO_" + filt + ".txt", [nm[inds]*10., foursao[inds]*500/nm[inds]*atm])
    writecol("CfA2_4ShooterHarris_" + filt + ".txt", [nm[inds]*10., fourharris[inds]*500/nm[inds]*atm])




    
color_txt = """
CfA2_AC_SAO_U	0.9312				
CfA2_AC_SAO_B	0.9293				
CfA2_AC_SAO_V	0.034				
CfA2_AC_SAO_R	0.9824				
CfA2_AC_SAO_I	1.0739	1.0639			1.0689
					
CfA2_AC_Harris_U	0.9617				
CfA2_AC_Harris_B	0.9631				
CfA2_AC_Harris_V	0.0441				
CfA2_AC_Harris_R	1.0947				
CfA2_AC_Harris_I	0.9899				
					
CfA2_4Sh_SAO_U	0.9433	0.965			0.95415
CfA2_4Sh_SAO_B	0.8937	0.883			0.88835
CfA2_4Sh_SAO_V	0.0423	0.0398			0.04105
CfA2_4Sh_SAO_R	0.9873	0.9685			0.9779
CfA2_4Sh_SAO_I	1.0837	1.0725	1.09		1.082066667
					
CfA2_4Sh_Harris_U	0.9638				
CfA2_4Sh_Harris_B	0.9155				
CfA2_4Sh_Harris_V	0.0447				
CfA2_4Sh_Harris_R	1.0812				
CfA2_4Sh_Harris_I	1.0284			        
""".split('\n')

color_terms = {}

for line in color_txt:
    parsed = line.split(None)
    if len(parsed) > 1:
        color_terms[parsed[0]] = float(parsed[-1])


U, B, V, R, I = 9.724, 9.907, 9.464, 9.166, 8.846

for key in ["CfA2_AC_SAO_", "CfA2_AC_Harris_", "CfA2_4Sh_SAO_", "CfA2_4Sh_Harris_"]:
    v_mag = color_terms[key + "V"]*(B - V) + V
    b_mag = color_terms[key + "B"]*(B - V) + v_mag
    u_mag = color_terms[key + "U"]*(U - B) + b_mag
    r_mag = -color_terms[key + "R"]*(V - R) + v_mag
    i_mag = -color_terms[key + "I"]*(V - I) + v_mag

    print key + "U", u_mag
    print key + "B", b_mag
    print key + "V", v_mag
    print key + "R", r_mag
    print key + "I", i_mag

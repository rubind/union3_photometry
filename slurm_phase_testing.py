import glob
from commands import getoutput
import time



def count_jobs():
    return int(len(getoutput("squeue | grep drubin").split('\n')))

#new_im = fl.replace(".fits", "_same_scale.fits")

n_jobs = 150

pwd = getoutput("pwd")

SNe = glob.glob("phase_testing/*/*")
print(len(SNe), "SNe found")

runs = list(set(["/".join(item.split("/")[:-1]) for item in SNe]))

print("runs", runs)


checks = []

for run in runs:
    checks += getoutput("grep Check " + run + "/*/result_deriv.dat").split('\n')
checks = [item for item in checks if item.count("All|All|All") > 0]
checks = ["/".join(item.split("/")[:-1]) for item in checks]

print(checks[:5])


for i in range(len(SNe))[::-1]:
    if checks.count(SNe[i]):
        del SNe[i]


print(len(SNe), "SNe remanining")


n_jobs = min(n_jobs, len(SNe))

for i in range(n_jobs):
    
    f = open("tmp.sh", 'w')
    f.write("#!/bin/bash\n")
    f.write("#SBATCH --job-name=fit\n")
    f.write("#SBATCH --partition=shared\n")
    f.write("#SBATCH --time=0-12:00:00 ## time format is DD-HH:MM:SS\n")
    f.write("#SBATCH --nodes=1\n")
    f.write("#SBATCH --cpus-per-task=4\n")
    f.write("#SBATCH --mem=4G # Memory per node my job requires\n")
    f.write("#SBATCH --error=example-%A.err # %A - filled with jobid, where to write the stderr\n")
    f.write("#SBATCH --output=example-%A.out # %A - filled with jobid, wher to write the stdout\n")
    f.write("source ~/.bash_profile\n")
    for SN in SNe[i::n_jobs]:
        f.write("cd " + pwd + "/" + SN + '\n')
        f.write("python /home/drubin/snfit_fitting//python_code/SALT3.py lc2*dat\n")
    f.close()

    print(getoutput("sbatch tmp.sh"))
    time.sleep(1)


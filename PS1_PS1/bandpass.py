from FileRead import readcol, writecol
from numpy import *
import pylab


[nm, op, g, r, i, z, y, w] = readcol("apj425122t3_mrt.txt", 'f,fffffff')

lambs = nm*10.

pylab.plot(lambs, op)
pylab.plot(lambs, g)
pylab.plot(lambs, r)
pylab.plot(lambs, i)
pylab.plot(lambs, z)
pylab.plot(lambs, y)
pylab.plot(lambs, w)

pylab.show()


writecol("PS_w.txt", [lambs, w])
writecol("PS_g.txt", [lambs, g])
writecol("PS_r.txt", [lambs, r])
writecol("PS_i.txt", [lambs, i])
writecol("PS_z.txt", [lambs, z])
writecol("PS_y.txt", [lambs, y])

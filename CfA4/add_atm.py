from numpy import *
from FileRead import readcol, writecol
import glob
from scipy.interpolate import interp1d


for fl in glob.glob("CfA4_Keplercam_B_*.txt"):
    [l,t] = readcol(fl, 'ff')
    [la, ta] = readcol("kpnoextinct.txt", 'ff')
    
    atmfn = interp1d(la, ta, kind = 'linear', fill_value = 0., bounds_error = False)

    writecol(fl.replace(".txt", "_atm.txt"), [l, t*atmfn(l)])

    

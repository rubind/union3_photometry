import glob
import subprocess
from FileRead import read_param, readcol
import matplotlib.pyplot as plt
import numpy as np
import tqdm

def do_it(cmd):
    print(cmd)
    subprocess.getoutput(cmd)
    

def get_early_Swope_SNe():
    all_v1_SNe = subprocess.getoutput("cat ../Union3/*v1.txt").split('\n')
    all_v1_SNe = set([item.replace("$UNION", "../Union3").strip() for item in all_v1_SNe])

    print("all_v1_SNe", len(all_v1_SNe))

    lc_files = glob.glob("../Union3/*/*/lc2fit*SWOPE*dat")
    print(len(lc_files), "lc files found")

    SN_paths = set(["/".join(item.split("/")[:-1]) for item in lc_files])
    print(len(SN_paths), "SN paths found", SN_paths)

    SN_paths = SN_paths.intersection(all_v1_SNe)

    print(len(SN_paths), "SN paths found from v1")

    early_SNe = []

    all_first_phase = []

    for SN_path in SN_paths:
        earliest_obs = 1e10

        for fl in glob.glob(SN_path + "/lc2fit*SWOPE*dat"):
            [x, y] = readcol(fl, 'ff')
            earliest_obs = min(earliest_obs, x.min())

        first_phase = (earliest_obs - read_param(SN_path + "/result_salt2.dat", "DayMax"))/(1. + read_param(SN_path + "/result_salt2.dat", "Redshift"))
        all_first_phase.append(first_phase)

        if first_phase < -5:
            early_SNe.append(SN_path)

    plt.hist(all_first_phase)
    plt.savefig("all_first_phase.pdf")
    plt.close()

    print(len(early_SNe), "early_SNe")

    return early_SNe


def get_early_SNe():
    all_v1_SNe = []
    for v1_fl in glob.glob("../Union3/*v1.txt"):
        [v1_SNe] = readcol(v1_fl, 'a')
        all_v1_SNe += v1_SNe
        
    all_v1_SNe = set([item.replace("$UNION", "../Union3").strip() for item in all_v1_SNe])

    early_SNe = []
    all_first_phase = []

    for SN_path in all_v1_SNe:
        earliest_obs = 1e10
        latest_obs = -1e10

        for fl in glob.glob(SN_path + "/lc2fit*dat"):
            [date, flux, dflux, zp] = readcol(fl, 'ffff')

            if len(date) == 0:
                [date, mag, dmag] = readcol(fl, 'fff')
            else:
                dmag = (2.5/np.log(10.))*dflux/flux
                

            inds = np.where((dmag < 0.05)*(dmag > 0))
            if len(inds[0]) > 9:
                earliest_obs = date[inds].min()
                latest_obs = date[inds].max()

        if earliest_obs != 1e10 and latest_obs != -1e10:
            first_phase = (earliest_obs - read_param(SN_path + "/result_salt2.dat", "DayMax"))/(1. + read_param(SN_path + "/result_salt2.dat", "Redshift"))
            last_phase = (latest_obs - read_param(SN_path + "/result_salt2.dat", "DayMax"))/(1. + read_param(SN_path + "/result_salt2.dat", "Redshift"))

            X1 = read_param(SN_path + "/result_salt2.dat", "X1")
            c = read_param(SN_path + "/result_salt2.dat", "Color")
            
            all_first_phase.append(first_phase)
            if first_phase < -10 and last_phase > 25 and (np.abs(X1) < 3) and (np.abs(c) < 0.3):
                early_SNe.append(SN_path)

    plt.hist(all_first_phase, bins = 30)
    plt.savefig("all_first_phase.pdf")
    plt.close()
                
    print(len(early_SNe), "early_SNe")
    return early_SNe

def copy_over_files(early_SNe, first_phase, last_phase, SNRmax):
    dr_name = "First_%02i_Last_%02i_SNRmax_%03i" % (first_phase, last_phase, SNRmax)

    do_it("rm -fr " + dr_name)
    do_it("mkdir " + dr_name)

    for SN in early_SNe:
        wd = dr_name + "/" + SN.split("/")[-1]
        do_it("mkdir " + wd)
        
        do_it("cp " + SN + "/lightfile " + wd)

        DayMax = read_param(SN + "/result_salt2.dat", "DayMax")
        redshift = read_param(SN + "/result_salt2.dat", "Redshift")

        min_date = DayMax + first_phase*(1. + redshift)
        max_date = DayMax + last_phase*(1. + redshift)
        

        for fl in glob.glob(SN + "/lc2fit_*dat"):
            f = open(fl, 'r')
            lines = f.read().split('\n')
            f.close()

            f = open(wd + "/" + fl.split("/")[-1], 'w')
            for line in lines:
                if line.count("@WEIGHTMAT") == 0:
                    parsed = line.split(None)
                    if len(parsed) == 3 or len(parsed) == 4:
                        try:
                            this_date = float(parsed[0])
                            if this_date >= min_date and this_date <= max_date:
                                f.write(line + '\n')
                        except:
                            f.write(line + '\n')
                    else:
                        f.write(line + '\n')
                    
            f.close()
            

            
do_it("rm -fr First_*_Last_*_SNRmax_*")

early_SNe = get_early_SNe()

print("early_SNe", early_SNe)

SN_names = [item.split("/")[-1] for item in early_SNe]
assert len(set(SN_names)) == len(SN_names)


for first_phase in tqdm.tqdm(np.arange(-10, 10.1, 1.)):
    for last_phase in np.arange(0., 20.1, 1.):
        if last_phase - first_phase > 5:
            copy_over_files(early_SNe, first_phase = first_phase, last_phase = last_phase, SNRmax = 500)
    
copy_over_files(early_SNe, first_phase = -30, last_phase = 60, SNRmax = 500)

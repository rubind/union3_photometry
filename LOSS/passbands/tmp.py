import commands
import glob

for fl in glob.glob("Passbands*eps"):
    print fl
    newfl = fl.replace(".eps", ".fits")

    assert newfl != fl, ".eps not found!"
    commands.getoutput("convert -type Grayscale -density 300 %s %s" % (fl, newfl))

    

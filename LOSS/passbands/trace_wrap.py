import commands
from FileRead import readcol, writecol
from numpy import *
import pylab

repl = [("K1", "B", "187,564"),
        ("K1", "V", "314,105"),
        ("K1", "R", "417,565"),
        ("K1", "I", "562,105"),

        ("K2", "B", "186,564"),
        ("K2", "V", "323,105"),
        ("K2", "R", "427,567"),
        ("K2", "I", "575,105"),

        ("K3", "B", "182,562"),
        ("K3", "V", "315,105"),
        ("K3", "R", "427,567"),
        ("K3", "I", "575,106"),

        ("K4", "B", "183,561"),
        ("K4", "V", "346,109"),
        ("K4", "R", "419,565"),
        ("K4", "I", "630,109"),

        ("N", "B", "189,564"),
        ("N", "V", "331,109"),
        ("N", "R", "414,566"),
        ("N", "I", "569,112")]

vert = ["140,560,0.0  140,944,1.0", "137,104,0.0  140,488,1.0"] # B/R and V/I


f = open("paramfile.txt")
orig_lines = f.read()
f.close()

for item in repl:
    print item

    fitsfile = "Passbands_%s_%s.fits" % ("BV"*("BV".count(item[1])) + "RI"*("RI".count(item[1])), item[0])
    outputfl = "LOSS_%s_%s.txt" % (item[0], item[1])

    lines = orig_lines.replace("sssss", item[2]).replace("fffff", fitsfile).replace("ooooo", outputfl)
    if item[1] == "B" or item[1] == "R":
        lines = lines.replace("vvvvv", vert[0])
    elif item[1] == "V" or item[1] == "I":
        lines = lines.replace("vvvvv", vert[1])
    else:
        time_to_stop

    f = open("new_paramfile.txt", 'w')
    f.write(lines)
    f.close()
    
    print commands.getoutput("python ../../../trace.py new_paramfile.txt")

    [l,t] = readcol(outputfl, 'ff')

    
    stop_here = None
    

    for threshold in [0.02, 0.05]:
        went_above = 0
        for i in range(len(l) - 2):
            if t[i] > threshold:
                went_above = 1
            if t[i] <= threshold and went_above == 1 and stop_here == None:
                if t[i+1] > t[i]:
                    stop_here = i+1
    

    writecol(outputfl, [l[:stop_here], t[:stop_here]])
    pylab.subplot(2,1,1)
    pylab.plot(l[:stop_here], t[:stop_here])
    pylab.subplot(2,1,2)
    pylab.plot(l[:stop_here], t[:stop_here])
    pylab.ylim(-0.01, 0.05)
    pylab.show()

    

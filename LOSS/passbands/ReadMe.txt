J/ApJS/190/418         Light curves for 165 SNe           (Ganeshalingam+, 2010)
================================================================================
Results of the Lick observatory supernova search follow-up photometry program:
BVRI light curves of 165 type Ia supernovae.
    Ganeshalingam M., Li W., Filippenko A.V., Anderson C., Foster G.,
    Gates E.L., Griffith C.V., Grigsby B.J., Joubert N., Leja J., Lowe T.B.,
    Macomber B., Pritchard T., Thrasher P., Winslow D.
   <Astrophys. J. Suppl. Ser., 190, 418-448 (2010)>
   =2010ApJS..190..418G
================================================================================
ADC_Keywords: Supernovae ; Photometry
Keywords: galaxies: distances and redshifts - supernovae: general

Abstract:
    We present BVRI light curves of 165 Type Ia supernovae (SNe Ia) from
    the Lick Observatory Supernova Search follow-up photometry program
    from 1998 through 2008. Our light curves are typically well sampled
    (cadence of 3-4 days) with an average of 21 photometry epochs. We
    describe our monitoring campaign and the photometry reduction pipeline
    that we have developed. Comparing our data set to that of Hicken et
    al., with which we have 69 overlapping supernovae (SNe), we find that
    as an ensemble the photometry is consistent, with only small overall
    systematic differences, although individual SNe may differ by as much
    as 0.1 mag, and occasionally even more. Such disagreement in specific
    cases can have significant implications for combining future large
    data sets. We present an analysis of our light curves which includes
    template fits of light-curve shape parameters useful for calibrating
    SNe Ia as distance indicators. Assuming the B-V color of SNe Ia at 35
    days past maximum light can be presented as the convolution of an
    intrinsic Gaussian component and a decaying exponential attributed to
    host-galaxy reddening, we derive an intrinsic scatter of
    {sigma}=0.076+/-0.019mag, consistent with the Lira-Phillips law. This
    is the first of two papers, the second of which will present a
    cosmological analysis of the data presented herein.

Description:
    The images in our data set were acquired using the 0.76m KAIT and the
    1m Nickel telescope, both at Lick Observatory located on Mt. Hamilton
    just outside of San Jose, CA. The site typically has an average seeing
    of ~2", with some seasonal dependence. We make an effort to
    spectroscopically classify and monitor newly discovered SNe with time
    allocated to us on the 3m Shane telescope at Lick Observatory using
    the Kast double spectrograph.

File Summary:
--------------------------------------------------------------------------------
 FileName   Lrecl  Records   Explanations
--------------------------------------------------------------------------------
ReadMe         80        .   This file
sne.dat        36      165   SNe positions from Simbad
table6.dat     48    13793   Photometry (165 SNe)
table7.dat    107     2468   Comparison stars (165 SNe)
--------------------------------------------------------------------------------

See also:
  J/AJ/112/2408    : Light Curves of 29 SNe (Hamuy+ 1996)
  J/MNRAS/389/1577 : UBVRI absolute magnitudes of Type Ia SNe (Takanashi+, 2008)

Byte-by-byte Description of file: sne.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label     Explanations
--------------------------------------------------------------------------------
   1- 15  A15   ---     SN        Supernova identification
  17- 18  I2    h       RAh       Simbad hour of right ascension (J2000)
  20- 21  I2    min     RAm       Simbad minute of right ascension (J2000)
  23- 26  F4.1  s       RAs       Simbad second of right ascension (J2000)
      28  A1    ---     DE-       Simbad declination sign (J2000)
  29- 30  I2    deg     DEd       Simbad degree of declination (J2000)
  32- 33  I2    arcmin  DEm       Simbad arcminute of declination (J2000)
  35- 36  I2    arcsec  DEs       Simbad arcsecond of declination (J2000)
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table6.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label     Explanations
--------------------------------------------------------------------------------
   1- 15  A15   ---     SN        Supernova identification
  17- 26  F10.2 d       JD        Julian date
      28  A1    ---     Filter    [BVRI] Filter used in the observation
  30- 35  F6.3  mag     mag       Observed magnitude in Filter
  37- 41  F5.3  mag   e_mag       Uncertainty in mag
  43- 48  A6    ---     Tel       Telescope used in the observation
--------------------------------------------------------------------------------

Byte-by-byte Description of file: table7.dat
--------------------------------------------------------------------------------
   Bytes Format Units   Label   Explanations
--------------------------------------------------------------------------------
   1- 15  A15   ---     SN      Supernova identification
  17- 19  I3    ---     Seq     Comparison star number
  21- 22  I2    h       RAh     Hour of Right Ascension (J2000)
  24- 25  I2    min     RAm     Minute of Right Ascension (J2000)
  27- 31  F5.2  s       RAs     Second of Right Ascension (J2000)
      33  A1    ---     DE-     Sign of the Declination (J2000)
  34- 35  I2    deg     DEd     Degree of Declination (J2000)
  37- 38  I2    arcmin  DEm     Arcminute of Declination (J2000)
  40- 43  F4.1  arcsec  DEs     Arcsecond of Declination (J2000)
  45- 50  F6.3  mag     Bmag    The B band magnitude
  52- 56  F5.3  mag   e_Bmag    Uncertainty in Bmag
  58- 59  I2    ---   o_Bmag    Number of observations to determine Bmag
  61- 66  F6.3  mag     Vmag    The V band magnitude
  68- 72  F5.3  mag   e_Vmag    Uncertainty in Vmag
  74- 75  I2    ---   o_Vmag    Number of observations to determine Vmag
  77- 82  F6.3  mag     Rmag    The R band magnitude
  84- 88  F5.3  mag   e_Rmag    Uncertainty in Rmag
  90- 91  I2    ---   o_Rmag    Number of observations to determine Rmag
  93- 98  F6.3  mag     Imag    The I band magnitude
 100-104  F5.3  mag   e_Imag    Uncertainty in Imag
 106-107  I2    ---   o_Imag    Number of observations to determine Imag
--------------------------------------------------------------------------------

History:
    From electronic version of the journal
================================================================================
(End)                 Greg Schwarz [AAS], Emmanuelle Perret [CDS]    25-Oct-2010

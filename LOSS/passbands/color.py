from numpy import *

CBV = """-0.095 0.027
-0.085 0.032
-0.057 0.032
-0.134 0.051
-0.092 0.053""".split('\n')

CRI = """-0.181 -0.071
0.062 -0.007
0.064 -0.001
0.107 0.014
0.089 -0.044""".split('\n')

CBV = [[float(item) for item in item2.split(None)] for item2 in CBV]
CRI = [[float(item) for item in item2.split(None)] for item2 in CRI]


B = 9.907
V = 9.464
R = 9.166
I = 8.846



for i, instr in enumerate(["LOSS_K1", "LOSS_K2", "LOSS_K3", "LOSS_K4", "LOSS_N"]):
    """b=B+CB(B-V)+ constant, v=V +CV(B-V)+ constant,
r =R+CR(V -R)+ constant,and
i = I + CI (V - I ) + constant.
"""
    print "LOSS %s %f" % (instr + "_B", B + CBV[i][0]*(B - V))
    print "LOSS %s %f" % (instr + "_V", V + CBV[i][1]*(B - V))
    print "LOSS %s %f" % (instr + "_R", R + CRI[i][0]*(V - R))
    print "LOSS %s %f" % (instr + "_I", I + CRI[i][1]*(V - I))

    

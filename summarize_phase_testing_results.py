import glob
import numpy as np
from FileRead import read_param
from Spectra import check_derivs
import pickle


def read_mB(SNpath):
    mB = read_param(SNpath + "/result_salt2.dat", "RestFrameMag_0_B")
    c = read_param(SNpath + "/result_salt2.dat", "Color")
    dc = read_param(SNpath + "/result_salt2.dat", "Color", ind = 2)
    x1 = read_param(SNpath + "/result_salt2.dat", "X1")
    dx1 = read_param(SNpath + "/result_salt2.dat", "X1", ind = 2)

    if (mB == None) or (x1 == None) or (dc == None) or (dc == 0):
        return None

    if np.abs(c) > 0.3:
        return None

    if np.abs(x1 + dx1) > 5:
        return None

    if dc > 0.2:
        return None


    return mB + 0.14*x1 - 3.1*c

    
    
orig_path = "phase_testing/First_-30_Last_60_SNRmax_500/"

orig_mu = {}

for SN in glob.glob(orig_path + "/*"):
    orig_mu[SN.split("/")[-1]] = read_mB(SN)

print("orig_mu", orig_mu)


delta_mu = {}

for SN in glob.glob("phase_testing/*/*"):
    if SN.count(orig_path) == 0:
        key = SN.split("/")[1]
        mu = read_mB(SN)
        
        if mu != None:
            dmu = mu - orig_mu[SN.split("/")[-1]]
            
            if key in delta_mu:
                delta_mu[key].append(dmu)
            else:
                delta_mu[key] = [dmu]

print("delta_mu", delta_mu)
for key in delta_mu:
    delta_mu[key] = np.array(delta_mu[key])

pickle.dump(delta_mu, open("delta_mu.pickle", 'wb'))

